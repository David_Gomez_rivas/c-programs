#include <stdio.h>
#include <stdlib.h>

/*
 * Este programa está hecho con propósitos
 * depurativos
 */

int suma (int op1, int op2) {
    return op1 + op2;
}

void
incrementa (int *variable, int cantidad) {
    *variable += cantidad;
}

int
main (int argc, char *argv[])
{
    int a = 2,
        b = 5,
        c;

    /* PASO POR VALOR */
    // En la pila se empujan siempre valores.
    // Por eso, las funciones pueden realizar
    // cálculos pero no pueden cambiar las
    // variables originales.
    c = suma (2 * a + 3, b);
    printf ("c = %i\n", c);

    /* PASO POR REFERENCIA */
    // Para cambiar el valor de a dentro de
    // una función puedo pasar la dirección
    // de memoria en la que está a en vez de
    // pasar el contenido de la variable a.
    incrementa (&a, 5);
    printf ("a = %i\n", a);

    return EXIT_SUCCESS;
}
