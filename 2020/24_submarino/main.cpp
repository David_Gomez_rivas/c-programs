#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#include "ansi.h"
#include "window.h"
#include "game_object.h"
#include "stack.h"


/*
 *
 * Struct (empaquetar variables)
 * Múltiples ficheros
 * Makefiles
 * Ficheros
 *
 * Estructuras de Datos:
 *   - Pilas
 *   - Colas
 *   - Listas enlazadas
 *   - Mapas (Hash - Diccionario)
 *
 * ECMS (Entity Component Manager System)
 * */


/* GAME LOGIC */
int
finished (){
    return 0;
}

struct TMovil *
data_init ()
{
    struct TMovil *newgo = (struct TMovil *) malloc (sizeof (struct TMovil));

    newgo->pos.x = 1;
    newgo->pos.y = random () % MX_W;
    newgo->vel.x = random () % 2 + 1;
    newgo->vel.y = 0;

    return newgo;
}

// todo: Avoid passing stack values. Use reference.
void
update_ph (struct TStack st)
{
    for (unsigned i=0; i<st.summit; i++)
        st.data[i]->pos.x += st.data[i]->vel.x;
}

void create_uh (struct TStack *go)
{
    static unsigned veces = 0;

    if (veces % 10 == 0)
        if (rand () % 2 == 0)
            push (go, data_init ());
}

void
data_destroy (struct TStack *st, unsigned pos)
{

    free (st->data[pos]);
    for (unsigned p=pos; p<st->summit-1; p++)
        st->data[p] = st->data[p+1];

    st->summit --;

    // free (st->data[pos]);
    // memmove (&st->data[pos], &st->data[pos+1], (st->summit - (pos + 1 )) * sizeof (struct TMovil *));
    // st->summit --;

    // st->data + pos  == &st->data[pos]
}

void
collect_garbage (struct TStack *st)
{
    for (unsigned i=0; i<st->summit; i++)
        if (st->data[i]->pos.x > MX_W - 2)
            data_destroy (st, i);
}


// todo: use references
void
draw (struct TStack gameobj)
{

    for (unsigned i=0; i<gameobj.summit; i++)
        print ( gameobj.data[i]->pos.x, gameobj.data[i]->pos.y, "🚣" );
}

/* MAIN LOOP */
int
main (int argc, char *argv[])
{
    struct TStack gameobj;

    srand (time (NULL));
    init (&gameobj);

    push (&gameobj, data_init ());

    ANSI (CURSOR_OFF);
    title ();
    sleep (2);
    while (!finished ()) {
        clear_scr ();
        frame (MX_W, MX_H);
        update_ph (gameobj);
        collect_garbage (&gameobj);
        draw (gameobj);
        create_uh (&gameobj);
        usleep ( 100000 );
    }

    ANSI (CURSOR_ON);

    return EXIT_SUCCESS;
}
