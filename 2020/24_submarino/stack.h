#ifndef __STACK_H__
#define __STACK_H__

#include <strings.h>

#include "game_object.h"

#define MAXSTACK 20

typedef struct TMovil * data_type;

enum TStackStatus {normal, full, empty};

struct TStack {
    data_type data[MAXSTACK];
    unsigned summit;
    enum TStackStatus status;
};


#ifdef __cplusplus
extern "C" {
#endif

    void init (struct TStack *stack);
    bool push (struct TStack *stack, data_type new_input);
    data_type pop (struct TStack *stack);

#ifdef __cplusplus
}
#endif


#endif
