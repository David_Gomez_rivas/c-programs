#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__

struct TVec {
    int x;
    int y;
};


struct TMovil {
    struct TVec pos;
    struct TVec vel;
};


#endif
