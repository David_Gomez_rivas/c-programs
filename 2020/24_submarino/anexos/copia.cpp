#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TEmpleado {
    char *nombre;
    double sueldo;
};

int
main (int argc, char *argv[])
{

    struct TEmpleado e0, e1;

    printf ("Nombre: ");
    scanf  (" %ms", &e0.nombre);
    printf ("Sueldo: ");
    scanf  (" %lf", &e0.sueldo);

    e1 = e0;

    printf ("%s [%.2lf]\n", e0.nombre, e0.sueldo);
    printf ("%s [%.2lf]\n", e1.nombre, e1.sueldo);

    strcpy (e1.nombre, "Manolo");
    e1.sueldo = 3000;

    printf ("%s [%.2lf]\n", e0.nombre, e0.sueldo);
    printf ("%s [%.2lf]\n", e1.nombre, e1.sueldo);


    free (e0.nombre);

    return EXIT_SUCCESS;
}
