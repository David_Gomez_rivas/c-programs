#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{

    /*
     * La inicialización sólo es posible en la declaración.
     * Se puede dejar el primer corchete vacío porque el
     * preprocesador cuenta y lo rellena automáticamente.
     * */
    char nombre[] = "Juenmari Atutxa";
    char n[] = { 'J', 'u', 'a', 'n', '\0' };

    nombre[2] = 'a';

    for (char *p=nombre; *p!='\0'; p++)
        printf ("%c", *p);

    printf ("\n");

    return EXIT_SUCCESS;
}
