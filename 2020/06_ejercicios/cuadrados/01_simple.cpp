#include <stdio.h>
#include <stdlib.h>

unsigned
pedir_lado () {
    unsigned lado;

    printf ("lado: ");
    scanf (" %u", &lado);

    return lado;
}


int
main (int argc, char *argv[])
{
    unsigned l;

    l = pedir_lado ();

    for (unsigned fila=0; fila<l; fila++) {
        for (unsigned col=0; col<l; col ++)
            printf ("*");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
