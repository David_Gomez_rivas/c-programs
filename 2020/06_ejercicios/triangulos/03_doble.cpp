/*
 * =====================================================================================
 *
 *       Filename:  03_doble.cpp 1.0 19/01/21 16:36:26
 *
 *    Description:  Solución al ejercicio 12
 *    sea h=5
 *
 *         **(CR)
 *        ****(CR)
 *       ******(CR)
 *      ********(CR)
 *     **********(CR)
 *
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>






void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay TRIANGULOS");
    printf ("\n\n\n");
}



unsigned
pedir_altura ()
{
    unsigned altura;

    printf ("Altura: ");
    scanf (" %u", &altura);

    return altura;
}


void
triangulo (unsigned h)
{
    for (unsigned f=0; f<h; f++) {
        /* Parte Izquierda */
        for (unsigned c=0; c<h; c++)
            if ( f + c < h - 1 )
                printf (" ");
            else
                printf ("*");

        /* Parte Derecha */
        for (unsigned c=0; c<f+1; c++)
                printf ("*");

        printf ("\n");
    }

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{

    titulo ();
    triangulo (pedir_altura ());

    return EXIT_SUCCESS;
}
