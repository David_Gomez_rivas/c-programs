/*
 * =====================================================================================
 *
 *       Filename:  entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Todo el código que cumpla una única función lo vamos a a islar
 *                  en funciones. Vamos a empezar por el título.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN         1
#define MAX      1000


/* Cuida de que visualmente cada idea ocupe un párrafo. */
void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}







void
poner_resultado (unsigned posible_primo)
{
    printf (
            "\n\n\t"                             // Cuando entre dos cadenas sólo median
                                                 // whitespaces (espacios, tabuladores y
                                                 // saltos de línea) se considera que es
                                                 // la misma cadena de caracteres.
                                                 // Acuérdate de que los comentarios son
                                                 // escardados por el preprocesador.
            "Has introducido el número %i\n\n"
            , posible_primo);
}







unsigned  // Esta función va a retornar un unsigned int que quedará como valor de retorno
          // en la poda del árbol gramatical.
pedir_numero ()
{
    unsigned posible_primo;   // Esta variable es distinta de la variable posible_primo
                              // que pertenece a la función main. Ésta reside en la pila
                              // de pedir numero (Acuérdate: [BP + offset] ).
    printf ("Número [%i - %i]: ", MIN, MAX);
    scanf ("%u", &posible_primo);


    // Las instrucciones terminan en el ;. El salto de línea no termina la instrucción.
    return    /* Cunado se alcanza un return se sale de la función y la ejecución retorna
                 a la función llamante.

                return puede igualmente dejar un valor de retorno para la poda.
               */
        posible_primo;
}





    int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo;


    /* ENTRADA Y SALIDA DE DATOS */
    /* Cuando diseñamos un algoritomo lo hacemos habitualmente
     * con este nivel de abstracción. Ponemos las cosas que
     * queremos que haga y luego en las funciones vamos pensando
     * de una en una cómo se hacen. Esto se llama análisis top down
     * o también análisis descendente. El procesos de desarrollos se
     * simplifica mucho y permite que todo se vea más claro.
     */
    do {
        titulo ();
        posible_primo = pedir_numero ();
    } while (posible_primo < MIN || posible_primo > MAX);

    /* CÁLCULOS */


    /* SALIDA DE DATOS */
    poner_resultado (posible_primo);

    return EXIT_SUCCESS;
}
