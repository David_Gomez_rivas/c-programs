#include <stdio.h>
#include <stdlib.h>

#define F 8
#define C 8

int
main (int argc, char *argv[])
{
    /*
     * NOTAS:
     *    1. Sólo se deben usar valores de F y C constantes.
     *       Linux soporta variables pero el código no sería portable.
     *    1. Sólo se pueden dar valores iniciales en la declaración.
     *    1. Se puede dejar el valor del primer corchete vacío porque
     *       el preprocesador contará los datos con los que inicializamos
     *       y lo rellenará automáticamente.
     *    */
    char tablero[F][C] = {
        { 'O', 'O', 'O', 'O', '1', '1', '1', '1'  },
        { 'O', '1', '1', '1', 'O', 'O', 'O', '1'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'  },
        { 'O', '1', '1', '1', '1', '1', 'O', 'O'  }
    };

    /* IMPRIMIR EL ARRAY EN PANTALLA */
    for (int f=0; f<F; f++) {
        for (int c=0; c<C; c++)
            printf (" %c", tablero[f][c]);
        printf ("\n");
    }

    /*
     * Nota de Javi:
     * Si cambiamos el for de la f por el de la c
     * la matriz se imprime traspuesta.
     * */

    printf ("\n");

    return EXIT_SUCCESS;
}
