#include <stdio.h>
#include <stdlib.h>

#define F 8
#define C 8

void
imprimir (char t[F][C])
{
    /* IMPRIMIR EL ARRAY EN PANTALLA */
    for (int f=0; f<F; f++) {
        for (int c=0; c<C; c++)
            printf (" %c", t[f][c]);
        printf ("\n");
    }
}

void
preg_disparo (int f, int c, int turno){
    printf ("Jugador %i\n", turno % 2 + 1);

}

int
main (int argc, char *argv[])
{
    int turno = 0;
    int fd, cd;

    char tablero[F][C] = {
        { 'O', 'O', 'O', 'O', '1', '1', '1', '1'  },
        { 'O', '1', '1', '1', 'O', 'O', 'O', '1'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'  },
        { 'O', '1', '1', '1', '1', '1', 'O', 'O'  }
    };

    while (1){
        turno ++;
        preg_disparo (fd, cd, turno);
        // Comporobar si le han disparado
    }

    imprimir (tablero);
    printf ("\n");

    return EXIT_SUCCESS;
}
