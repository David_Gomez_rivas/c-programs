#include <stdio.h>
#include <stdlib.h>

bool
es_borde (unsigned f, unsigned c, unsigned l) {
    return f == 0 || f == l-1 || c == 0 || c == l-1;
}

int
main (int argc, char *argv[])
{
    unsigned lado;

    printf ("l = ");
    scanf (" %u", &lado);

    for (unsigned f=0; f<lado; f++) {
        for (unsigned c=0; c<lado; c++)
            if (es_borde (f, c, lado))
                printf ("*");
            else
                printf (" ");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
