#include <stdio.h>
#include <stdlib.h>

int
dime_numero (const char *label)
{
    int n;

    printf ("Numero %s: ", label);
    scanf  (" %i", &n);

    return n;
}

void
pregunta (int *a, int *b)
{
    *a = dime_numero ("A");
    *b = dime_numero ("B");
}

int
main (int argc, char *argv[])
{
    int a,
        b,
        *menor,
        *mayor;

    pregunta (&a, &b);

    menor = &b;
    mayor = &a;
    if (a < b ) {
        menor = &a;
        mayor = &b;
    }

    printf ("Menor: %i\n", *menor);
    printf ("Mayor: %i\n", *mayor);

    return EXIT_SUCCESS;
}
