#include <stdio.h>
#include <stdlib.h>

void
portada ()
{
    system ("clear");
    system ("toilet -fpagga RUSSE");
}

void
preguntar_op (unsigned *op1, unsigned *op2)
{
    printf ("Operando 1:");
    scanf (" %u", op1);
    printf ("Operando 1 = %u\n", *op1);
    printf ("Operando 2:");
    scanf (" %u", op2);
    printf ("Operando 2 = %u\n", *op2);
}

bool es_impar (unsigned n) { return n % 2 != 0; }

void
resultado (unsigned op1, unsigned op2, unsigned total)
{

    printf ("\n\n"
            "\t=====================\n"
            "\t   %u x %u = %u\n"
            "\t=====================\n"
            "\n"
            , op1, op2, total);
}

int
main (int argc, char *argv[])
{
    unsigned op1, op2,
             ip1, ip2,
             suma = 0;
    portada ();
    preguntar_op (&op1, &op2);
    // Guardar el valor inicial de las variables.
    ip1 = op1;
    ip2 = op2;

    /* Multiplicar */
    while (op1 > 0) {
        if (es_impar (op1))
            suma += op2;
        op1 >>= 1;
        op2 <<= 1;
    }

    resultado (ip1, ip2, suma);

    return EXIT_SUCCESS;
}
