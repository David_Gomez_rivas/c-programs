#include <stdio.h>
#include <stdlib.h>

#define NCOEF 4
#define GRADO (NCOEF - 1)

#define STEP    .001
#define LI   -10.
#define LS    10.

#define TOTAL_PUNTOS (int) ( (LS - LI) / STEP )

enum TStrategy { st_text, st_curve};
const int print_strategy = st_text;

double
f (double p[NCOEF], double x) {
    double valor = 0;
    double pot   = 1;

    for (int coef=0; coef<NCOEF; coef++) {
        valor += pot * p[coef];
        pot *= x;
    }

    return valor;
}

/* Calculate the index associated to a given x */
int
x2i (double x) {
    return (int) ( (x - LI) / STEP );
}

void
print_text (double f1[TOTAL_PUNTOS], double f2[TOTAL_PUNTOS])
{

    for (double x=LI; x<LS; x+=STEP)
        printf ("%.4lf\t%.4lf\t%.4lf\n", x, f1[x2i (x)], f2[x2i (x)]);

    printf ("%.4lf\t%.4lf\n", LS, f1[x2i(LS)]);
}

void
print_curve (double f1[TOTAL_PUNTOS], double f2[TOTAL_PUNTOS])
{

}

void
print (double f1[TOTAL_PUNTOS], double f2[TOTAL_PUNTOS]) {
    switch (print_strategy) {
        case st_text:
            print_text (f1, f2);
            break;
        case st_curve:
            print_curve (f1, f2);
            break;
    }

}

void
find_zero (double func[ TOTAL_PUNTOS ], double zeros[NCOEF-1]) {
    int zfound = 0;

    for (double x=LI; x<LS; x+=STEP)
        if (func[ x2i (x) ] * func[ x2i (x+STEP) ] <= 0 )
            zeros[zfound++] = x;

}

int
main (int argc, char *argv[])
{
    double p[NCOEF] = { -3, 2, 5, 1 };
    double raster[ TOTAL_PUNTOS + 1];
    double deriva[ TOTAL_PUNTOS ];
    double zeros[NCOEF-1], maxmin[NCOEF-1];


    // Rellenar los valores de la función
    for (double x=LI; x<LS + STEP; x+=STEP)
        raster[x2i (x)] = f(p, x);

    // Calcular la derivada
    for (double x=LI; x<LS; x+=STEP)
        deriva[x2i (x)] = (raster[ x2i (x+STEP) ] - raster[ x2i (x)] ) / STEP;

    find_zero (raster, zeros);
    find_zero (deriva, maxmin);

    for (int z=0; z<NCOEF-1; z++)
        printf ("Zero: %.4lf\n", zeros[z]);

    for (int z=0; z<NCOEF-1; z++)
        printf ("Maxmin: %.4lf\n", maxmin[z]);


    //print (raster, deriva);

    return EXIT_SUCCESS;
}
