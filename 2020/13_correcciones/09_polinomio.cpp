#include <stdio.h>
#include <stdlib.h>

#define NCOEF 6
#define GRADO (NCOEF - 1)


double
f (double p[NCOEF], double x) {
    double valor = 0;
    double pot   = 1;

    for (int coef=0; coef<NCOEF; coef++) {
        valor += pot * p[coef];
        pot *= x;
    }

    return valor;
}

int
main (int argc, char *argv[])
{
    double p[NCOEF] = { 3, 2, 5, 1 };

    for (double x=-10; x<=10; x+=.1)
        printf ("%.4lf\t%.4lf\n", x, f(p, x));

    return EXIT_SUCCESS;
}
