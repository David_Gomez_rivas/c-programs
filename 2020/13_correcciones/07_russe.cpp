#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NOP 2

void
portada ()
{
    system ("clear");
    system ("toilet -fpagga RUSSE");
}

void
preguntar (unsigned op[NOP])
{
    for (int i=0; i<NOP; i++) {
        printf ("\n");
        printf ("Operando %i: ", i + 1);
        scanf (" %u", &op[i]);
        printf ("Operando %i = %u\n", i + 1, op[i]);
    }
}

bool es_impar (unsigned n) { return n % 2 != 0; }

    void
resultado (unsigned op1, unsigned op2, unsigned total)
{

    printf ("\n\n"
            "\t=====================\n"
            "\t   %u x %u = %u\n"
            "\t=====================\n"
            "\n"
            , op1, op2, total);
}

    int
main (int argc, char *argv[])
{
    unsigned op[NOP],
             ip[NOP],
             suma = 0;
    portada ();
    preguntar (op);
    // Guardar el valor inicial de las variables.
    memcpy (ip, op, NOP * sizeof(unsigned));

    /* Multiplicar */
    while (op[0] > 0) {
        if (es_impar (op[0]))
            suma += op[1];
        op[0] >>= 1;
        op[1] <<= 1;
    }

    resultado (ip[0], ip[1], suma);

    return EXIT_SUCCESS;
}
