#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

/*
 * +-----+             +-------+
 * |     |             |       |
 * |  ---+------------>|   ----+---->Guille
 * |     |             |       |
 * +-----+             +-------+
 *                     |       |
 *                     |   ----+---->Mario
 *                     |       |
 *                     +-------+
 *                     |       |
 *                     |   ----+---->Bernabe
 *                     |       |
 *                     +-------+
 *                     |       |
 *                     |   ----+---->Ramon
 *                     |       |
 *                     +-------+
 * */
int
main (int argc, char *argv[])
{

    char **amigos = NULL;
    char **p;
    int input = 0;
    int nnombres = 0;


    /* ENTRADA DE DATOS */
    do {
        system ("clear");
        amigos = (char **) realloc (amigos, (nnombres + 1) * sizeof (char *));
        *(amigos+nnombres) = (char *) 0;
        printf ("Amigo: ");
        input = scanf ("%m[a-z A-Z]", amigos + nnombres++ );
        __fpurge (stdin);
    } while (input);

    /* SALIDA DE DATOS */
    system ("clear");
    p = amigos;
    while (*p != NULL){
        printf ("%s\n", *p);
        p++;
    }

    /* LIMPIEZA */
    for (char **p=amigos; *p!=NULL; p++)
        free (*p);
    free (amigos);

    return EXIT_SUCCESS;
}
