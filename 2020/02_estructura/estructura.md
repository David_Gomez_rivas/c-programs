# Estructura de un Programa

Declarar: Asignar un nombre a una dirección de memoria y apuntar en la
tabla de símbolos los tipos de datos relacionados.

Definir: Decir qué datos componen una dirección de memoria.

## Variables

### Arquitectura

1. Punta delgada (little endian) 27 => 27000000
1. Punta gruesa  (big endian)    27 => 00000027

### Tipos

1. globales
1. locales     [BP-X]
1. parámetros  [BP+X]

Las locales y los parámetros viven en la pila.


## Funciones

Tienen:

1. Declaración: &lt;tipo&gt; &lt;id&gt; ([&lt;parametro&gt; [, &lt;parametros&gt;]*]) [^1 Notación de Backus-Naur]
1. Definición
1. Llamada
1. Dejan un valor de retorno

## Niveles semánticos

1. Compiladores
1. Intérpretes
1. bytecodes
1. Ens y & CM

## Micro

### Registros

1. IP (instruction Pointer)
1. BP (Base Pointer)
1. SP (Stack Pointer)
1. AX (Acumulador)
1. BX ()
1. CX ()
1. DX ()
1. SI (Source Index)
1. DI (Destiny Index)

## C
    1. Preprocesador
    1. Compilador & enlazador
    1. Librería Estándard
