#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define FILENAME "palabras.txt"

#define MAXCHAR 30


int
main (int argc, char *argv[])
{
    FILE *pf;
    char **lista = NULL;
    int total_words = 0;
    char buffer[MAXCHAR];
    int elegida;


    /* INICIALIZACIÓN */

    srand (time (NULL));

    // Apertura del Fichero
    pf = fopen (FILENAME, "r");

    if (!pf) {
        fprintf (stderr, "File %s not found.\n", FILENAME);
        return EXIT_FAILURE;
    }

    /* Lectura del fichero */
    while (!feof (pf)) {
        lista = (char **) realloc ( lista, (total_words + 1) * sizeof (char *) );
        fgets(buffer, MAXCHAR, pf);
        lista[total_words++] = (char *) malloc (strlen (buffer) + 1);
    }


    elegida = rand () % total_words;


/********************************************************************************/
/*                                                                              */
/*                              JUEGO                                           */
/*                                                                              */
/********************************************************************************/



   int nletras = strlen (lista[elegida]);
   char *adivi = (char *) malloc(nletras + 1);
   memset (adivi, '-', nletras);
   adivi[nletras] = '\0';



/********************************************************************************/
/*                                                                              */
/*                             FIN JUEGO                                        */
/*                                                                              */
/************************************

    /* Limpieza de la memoria */
    for (int i=0; i<total_words; i++)
        free (lista[i]);
    free (lista);

    fclose (pf);

    return EXIT_SUCCESS;
}
