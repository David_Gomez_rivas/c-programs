#include <stdio.h>
#include <stdlib.h>

#define N 5

int
main (int argc, char *argv[])
{
    char *palabra[N];

    /* ENTRADA DE DATOS */
    for (int i=0; i<N; i++){
        printf ("Nombre %i: ", i + 1);
        scanf (" %ms", &palabra[i]);
    }


    /* SALIDA DE DATOS */
    printf ("\n");
    for (int i=0; i<N; i++)
        printf ("Nombre %i: %s\n", i + 1, palabra[i]);
    printf ("\n");


    /* HOUSEKEEPING */
    for (int i=0; i<N; i++)
        free (palabra[i]);



    return EXIT_SUCCESS;
}
