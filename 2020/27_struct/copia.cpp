#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TPrueba {
    int a;
    double b;
    char n[20];
    char *p;
};

void
malvada (struct TPrueba j) {
    j.a = 5;
    j.b = 8.0;
    strcpy (j.n, "manolo");
    strcpy (j.p, "manolo");
}

int
main (int argc, char *argv[])
{
    struct TPrueba y = { 2, 3.4, "pepelu", NULL };

    y.p = (char *) malloc (20);
    strcpy (y.p, "pepelu");

    malvada (y);

    printf ("a: %i\n", y.a);
    printf ("b: %lf\n", y.b);
    printf ("n: %s\n", y.n);
    printf ("p: %s\n", y.p);

    return EXIT_SUCCESS;
}
