#include <stdio.h>
#include <stdlib.h>


void
multiplica (
        double *A, double *B, double *C,
        int ML, int KL,  int NL) {
    for (int i=0; i<ML; i++)
        for (int j=0; j<NL; j++) {
            *(C + i * NL + j) = 0;
            for (int k=0; k<KL; k++)
                *(C + i * NL + j) +=
                    *(A + i * KL + k) * *(B + k * NL + j);
        }


}

void
print (double *C, int ML, int NL)
{
    printf ("\n\n");
    for (int i=0; i<ML; i++){
        for (int j=0; j<NL; j++)
            printf ("%10.2lf",
                    *( C + i * NL + j));
        printf ("\n");
    }
    printf ("\n");


}
int
main (int argc, char *argv[])
{
    double *A;
    double *B;
    double *C;
    int m = 3, k = 4, n = 2;

    // todo: Pedir las dimensiones

    A = (double *) malloc (m * k * sizeof (double));
    B = (double *) malloc (k * n * sizeof (double));
    C = (double *) malloc (m * n * sizeof (double));

    // todo: rellenar las matrices

    printf ("\n");
    multiplica ( A, B, C, m, k, n);
    print (A, m, k);
    print (B, k, n);
    print (C, m, n);

    free (A);
    free (B);
    free (C);

    return EXIT_SUCCESS;
}
