#include <stdio.h>
#include <stdlib.h>

#define N 10

int
ticket()
{
    static int tckt_nb = 0;

    return ++tckt_nb;
}

int
main (int argc, char *argv[])
{

    for (int i=0; i<N; i++)
        printf ("Ticket %i\n", ticket() );

    return EXIT_SUCCESS;
}
