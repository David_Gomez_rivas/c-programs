#include <stdio.h>
#include <stdlib.h>

void
imprime_menores (int n) {
    printf ("%i\n", n);
    if (n>0)
        imprime_menores (n-1);
}


int
main (int argc, char *argv[])
{
    imprime_menores (10);

    return EXIT_SUCCESS;
}
