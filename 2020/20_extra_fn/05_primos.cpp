#include <stdio.h>
#include <stdlib.h>

bool
tiene_div_menores_que (int cand, int div) {
    if (div == 1)
        return false;
    return cand % div == 0 || tiene_div_menores_que (cand, div - 1);
}
int
main (int argc, char *argv[])
{
    int n;

    printf ("Número: ");
    scanf (" %i", &n);

    /* El 1 no es primo */
    /* Ojo con el 0*/
    /* Con los negativos no funciona */
    if (!tiene_div_menores_que (n , n / 2))
        printf ("Es primo %i.\n", n);
    else
        printf ("No es primo %i.\n", n);

    return EXIT_SUCCESS;
}
