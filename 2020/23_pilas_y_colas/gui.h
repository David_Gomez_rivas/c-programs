#ifndef __INPUT_H__
#define __INPUT_H__

#include "data_definition.h"
#include "stack.h"

#define XPRMPT 0
#define YPRMPT 17


#ifdef __cplusplus
extern "C" {
#endif

    unsigned ask (data_type *new_input);
    void title ();
    void print (struct TStack stack);
    void show (data_type n);

#ifdef __cplusplus
}
#endif

#endif
