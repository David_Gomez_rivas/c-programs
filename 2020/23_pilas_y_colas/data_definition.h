#ifndef __DATA_DEFINITION_H__
#define __DATA_DEFINITION_H__

#define MAXSTACK 10

/*DATA TYPES */
#define CHAR      0
#define INT       1
#define UNSIGNED  2
#define FLOAT     3
#define DOUBLE    4


#define DATA_TYPE UNSIGNED

#if DATA_TYPE == CHAR
typedef char data_type;

#elif DATA_TYPE == INT
typedef int data_type;

#elif DATA_TYPE == UNSIGNED
typedef unsigned data_type;

#elif DATA_TYPE == FLOAT
typedef float data_type;

#elif DATA_TYPE == DOUBLE
typedef double data_type;
#endif

#endif
