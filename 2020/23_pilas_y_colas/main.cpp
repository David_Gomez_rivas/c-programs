#include <stdlib.h>


#include "data_definition.h"
#include "gui.h"
#include "stack.h"

#include <stdio.h>

int
main (int argc, char *argv[])
{
    struct TStack stack;
    data_type new_input;

    int read;
    data_type ret;

    init (&stack);

    do {
        title ();
        print (stack);
        read = ask (&new_input);
        if (read != 0)
            push (&stack, new_input);
        else {
            ret = pop (&stack);
            if (stack.status == normal)
                show (ret);
        }

    } while (new_input != 0);

    return EXIT_SUCCESS;
}
