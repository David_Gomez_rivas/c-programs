#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <ctype.h>


#define MAXCHAR 0x20

void
ver_media (double media) {
    printf ("===============\n"
            "  Media: %.2lf\n"
            "===============\n"
            , media);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    int leidos = 0;
    char texto[MAXCHAR];
    unsigned veces = 0;
    unsigned entrada,
             suma = 0;


    do {
        printf ("Número %3u: ", veces+1);
        __fpurge (stdin);
        leidos = scanf ("%[0-9]", texto);

        entrada = atoi (texto);

        if (leidos) {
            veces++;
            suma += entrada;
        }

    } while (leidos);

    ver_media ( (double) suma / veces );

    return EXIT_SUCCESS;
}
