#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int
main (int argc, char *argv[])
{
    unsigned repeticiones = 0;
    char nombre[N];

    do {
        __fpurge (stdin);
        printf ("Repeticiones: ");
    } while (!scanf  (" %u", &repeticiones));

    printf ("Dime tu nombre: ");
    scanf  (" %s", nombre);

    for (unsigned i=0; i<repeticiones; i++)
        printf ("Tu nombre: %s.\n", nombre);


    for (unsigned i=0; i<strlen(nombre); i++)
        printf ("%c", nombre[i]);

    printf ("\n");

    return EXIT_SUCCESS;
}
