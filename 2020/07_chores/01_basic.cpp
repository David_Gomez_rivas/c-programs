#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

#define MAX 0x20

void
vaciar () {
    scanf (" %*1[/-]");
}

void
leer (unsigned *variable) {
    scanf (" %u", variable);
}

int
main (){
    unsigned rep=0;
    unsigned dia,
             mes,
             anio;
    char nombre[MAX];

    do {
        printf ("Repeticiones: ");
        __fpurge (stdin);
    } while ( !scanf (" %u", &rep) );

    printf ("Tu nombre: ");
    scanf (" %s", nombre);



    for (unsigned i=0; i<rep; i++)
        printf ("%s\n", nombre);

    /* Lectura con caracteres no interesantes */
    printf ("Nacimiento: ");
    scanf (" %u %*1[-/] %u %*1[-/] %u", &dia, &mes, &anio);

    printf (" %u/%u/%u\n", dia, mes, anio);

    /* Version 2*/

    printf ("Nacimiento: ");
    leer (&dia);
    vaciar ();
    leer (&mes);
    vaciar ();
    leer (&anio);

    printf (" %u/%u/%u\n", dia, mes, anio);

    return EXIT_SUCCESS;
}
