#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PAL_FINAL  "fin"

int
palabra_final (const char *palabra)
{
    return !strcasecmp (palabra, PAL_FINAL);

}


void
imprimir (char **lista, unsigned npl)
{
    for (unsigned i=0; i<npl; i++)
        printf ("%i.- %s\n", i+1, lista[i]);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    char **lista = NULL;
    unsigned npl = 0;

    printf ("Introduce una lista de palabras.\n");
    printf ("Escribe 'fin' para terminar.\n");
    printf ("\n");

    // PROBLEMA 1
    do {
        lista = (char **) realloc ( lista, (npl + 1) * sizeof (char *) );
        printf ("Palabra: ");
        scanf ( "%ms", &lista[npl++] );
        // scanf ( "%ms", lista + npl++ );
    } while (!palabra_final (lista[npl - 1]));



    // PROBLEMA 2
    //  Ordenar la lista de palabras.


    // PROBLEMA 3
    // Imprimir la lista
    imprimir (lista, npl);

    for (unsigned i=0; i<npl; i++)
        free (lista[i]);
    free (lista);

    return EXIT_SUCCESS;
}
