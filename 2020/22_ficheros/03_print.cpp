#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    FILE *pf = NULL;

    if (!(pf = fopen ("fortune.txt", "r"))){
        fprintf (stderr, "File not found.\n");
        return EXIT_FAILURE;
    }

    do {
        putchar (getc (pf));
    } while (!feof (pf));
    /* FIJAOS CÓMO SE VE EL EOF */

    fclose (pf);

    return EXIT_SUCCESS;
}
