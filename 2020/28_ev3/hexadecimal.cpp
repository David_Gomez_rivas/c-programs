#include <stdio.h>
#include <stdlib.h>

char digit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

void
hexa (unsigned n){
    if (n > 0)
        hexa ( n / 0x10);
    printf ("%c", digit[n % 0x10]);
}

int
main (int argc, char *argv[])
{
    unsigned decimal = 197;

    hexa (decimal);
    printf ("\n");

    return EXIT_SUCCESS;
}
