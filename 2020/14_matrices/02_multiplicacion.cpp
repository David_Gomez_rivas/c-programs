#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 3
#define N 2

void
pedir_datos (int *m, int filas, int cols, const char *label) {
    for (int f=0; f<filas; f++)
        for (int c=0; c<cols; c++) {
            printf("%s[%i][%i]: ", label, f+1, c+1);
            scanf (" %i", m + f * cols + c);
        }
}

int
main (int argc, char *argv[])
{
    int A[M][K];

    int B[K][N] = {
        { 1,  2},
        {-2,  1},
        { 1, -3}
    };

    int C[2][2];

    pedir_datos ((int *) A, M, K, "A");
    pedir_datos ((int *) B, K, N, "B");

    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++) {
            C[i][j] = 0;
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }

    return EXIT_SUCCESS;
}
