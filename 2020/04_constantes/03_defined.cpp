#include <stdio.h>
#include <stdlib.h>

/**
 * STANDARD DEFINED MACROS
 * __FILE__
 * __LINE__
 *
 * Desde el 99
 * __DATE__
 * __TIME__
 * __STDC__
 * __STDC_VERSION__
 * __STDC_HOSTED__
 * __cplusplus
 * __OBJC__
 * __ASSEMBLER__
 */
int
main (int argc, char *argv[])
{

    printf ("Hola, ¿cómo estás?\n");
    printf ("Esto es el fichero "      __FILE__ "\n");
    printf ("Estás en la línea %i\n",  __LINE__     );
    printf ("Esto es el fichero %s\n", __FILE__     );

    return EXIT_SUCCESS;
}
