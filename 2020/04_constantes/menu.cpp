#include <stdio.h>
#include <stdlib.h>



#define MENU "                     \n\
                                   \n\
         ************              \n\
         *   MENU   *              \n\
         ************              \n\
                                   \n\
  OPCIONES:                        \n\
                                   \n\
       1. Sumar                    \n\
       2. Restar                   \n\
       3. Multiplicar              \n\
       4. Dividir                  \n\
                                   \n\
  Tu Opción: \
"


/**********************/
/*   TIPOS DE DATOS   */
/**********************/

enum TOperacion { sumar, restar, multiplicar, dividir };






/************/
/*   MAIN   */
/************/

int
main (int argc, char *argv[])
{

    int opcion;           // Opción del menú.



    /* BLOQUE DE ENTRADAS */

    do {
        printf (MENU);
        scanf ("%i", &opcion);
        opcion--;
    } while (opcion > dividir);



    /* SALIDA DE DATOS */
    printf ("Has elegido: ");
    switch (opcion) {
        case sumar:
            printf("Sumar.");
            break;
        case restar:
            printf("Restar.");
            break;
    }
    printf ("\n");

    return EXIT_SUCCESS;
}
