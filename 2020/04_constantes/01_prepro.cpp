#include <stdio.h>
#include <stdlib.h>

/* El preprocesador es una herramienta de sustitución de textos */
/* El lenguaje del preprocesador se llama m4 */

#define EURO 166.386
#define PTAS 1 / EURO  /* Se puede hacer uso de definiciones previas */

// No se puede hacer:
// #define EURO 166.386  // Lo que vale 1 euro
//
// Sí se puede hacer:
// #define EURO 166.386 /* Lo que vale 1 Euro */

/* MACROS */

#define IMPRIME        printf /* Sustituimos las constantes por código */
/* La macro puede recibir un parámetro */
#define PON_ENTERO(x)  printf ("Entero: %i\n", (x))
#define SUMA2(x)       x + 2
/* Ó 2 parámetros (o más). */
#define SUMA(x,y)      x + y   /* Macro mal hecha */
#define SUMB(x,y)      ( (x) + (y) )

/* O un número variable de parámetros.*/
/* Macros variádicas */
#define ESCRIBE(...)   printf ("Macro Variádica: " __VA_ARGS__)
/* Si no te gusta __VA_ARGS__ */
#define ESCRI2(argumento...) printf ("Macro Variádica: " argumento)
/* Se puede pedir un argumento obligatorio */
#define ESCRI3(formato, ...) printf (formato, __VA_ARGS__ )


/********************/
/*   COSAS ÚTILES   */
/********************/

// Si entre dos cadenas de caracteres sólo median
// whitespaces (\n, \t, ' ') se considera
// la misma cadena de caracteres.

// Hay que poner todos los parámetros entre paréntesis
// y el resultado global de la macro entre paréntesis.

// Son útiles para poner textos grandes
#define LINEA "comando argumento1 argumento2"
#define AYUDA "        \n\
    01_prepro <argumentos>    \n\
                              \n\
  Este programa no funciona.  \n\
                              \n\
  Opcion1:                    \n\
                              \n\
"


int
main (int argc, char *argv[])
{
    IMPRIME ("El precio del euro en pesetas es: %lf\n", EURO);
    PON_ENTERO(2);
    PON_ENTERO(3);
    PON_ENTERO(SUMA2(5));   // El parámetro de PON_ENTERO es: 5 + 2
    PON_ENTERO(SUMA (2, 4));

    ESCRIBE ("%i\n", 2);
    /* La macro se expande así:
     * printf ("Macro Variádica: " "%i\n", 2);
     * */


    PON_ENTERO(SUMA (2, 4) * 6);
    /* Expansión:
     * printf ("Entero: %i\n", 2 + 4 * 6));
     */

    PON_ENTERO(SUMB (2, 4) * 6);

    IMPRIME (AYUDA);
    printf (
             "Esto es una linea\n"
             "de la ayuda.\n"
            );

    return EXIT_SUCCESS;
}
