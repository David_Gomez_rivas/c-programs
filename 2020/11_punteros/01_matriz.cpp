#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    // La inicialiación sólo se puede hacer cuando
    // se declara la variable.
    char nombre[5] = { 'p', 'i', 'p', 'e' };
    char n1[5] = { 'p', 0x61, 'p', 'e', '\0' };
    char n2[5] = "pepe";

    nombre[2] = 'b';  // Constante simbólica

    // Notación de matrices
    printf ("%c => %i\n", nombre[2], nombre[2]);

    // Notación de punteros
    printf ("%c => 0x%X\n", nombre[2], *(nombre + 2));

    printf ("%c", nombre[0]);
    printf ("%c", nombre[1]);
    printf ("%c", nombre[2]);
    printf ("%c", nombre[3]);
    printf ("\n");

    for (int i=0; i<4; i++)
        if (nombre[i] == 'i')
            printf ("!");
        else
            printf ("%c", nombre[i]);

    printf ("\n");

    printf ("%s\n", n1);
    printf ("%s\n", n2);

    return EXIT_SUCCESS;
}
