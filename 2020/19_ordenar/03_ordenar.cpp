#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*
Execute
/.02_ordena 2> /dev/null
*/

/* ANSI COLORS*/

#define GREEN "32m"
#define RESET "0m"
#define ANSI  "\x1B["

#define NDBG

#ifndef NDBG

#define DEBUG(...)                   \
    fprintf (stderr, ANSI GREEN );   \
    fprintf (stderr, __VA_ARGS__);   \
    fprintf (stderr, ANSI RESET );

#else

#define DEBUG(...)

#endif

void imprimir (int *numero, int cuantos, FILE *out) {
    if (!out)
        out = stdout;
    for (int i=0; i<cuantos; i++)
        fprintf (out, "%4i ", numero[i]);
    fprintf (out, "\n");
}



void
intercambia (int *num1, int *num2)
{
    int aux;

    aux = *num1;
    *num1 = *num2;
    *num2 = aux;
}




/* Quicksort */
void
ordena_4 (int *numero, int i, int j)
{
    DEBUG("\n[%i, %i]\n", i, j);

    if (j - i < 2) {
        if (numero[i] > numero[j])
            intercambia (&numero[i], &numero[j]);
        return;
    }

    int i0 = i, j0 = j;
    int cuantos = j - i + 1;
    int pivot = (numero[i] + numero[j] + numero[i+1]) / 3;
    DEBUG("Pivot: %i\n", pivot);

    while (i<j) {
        while (numero[i]<=pivot && i<=j)  // Todos los números pueden ser iguales e iguales al pivot.
            i++;
        while (numero[j]>pivot)           // El pivot siempre queda en la parte izquierda por el =.
            j--;
        if (i<j)
            intercambia (&numero[i++], &numero[j--]);
    }

#ifndef NDBG
    fprintf(stderr, ANSI GREEN);
    imprimir (numero+i0, cuantos, stderr);
    fprintf(stderr, ANSI RESET);
    DEBUG("(i, j) = (%i, %i)\n", i, j);
#endif

    if (j != j0) {                    // Todos los números son iguales
        ordena_4 (numero, i0, j);
        ordena_4 (numero, j+1, j0);
    }
}




/* Método de la burbuja */
void
ordena_3 (int *numero, int cuantos) {

    for (int veces=0; veces<cuantos-1; veces++)
        for (int pos=0; pos<cuantos-1; pos++)
            if (numero[pos+1] < numero[pos])
                intercambia (&numero[pos],
                        &numero[pos+1]);
}



/* Ordenamiento por selección */
void
ordena_2 (int *numero, int cuantos) {
    int cual, la_menor_pos;

    for (int menor=0; menor<cuantos-1; menor++){
        for (cual=menor+1,
                la_menor_pos=menor; cual<cuantos; cual++)
            if (numero[cual] < numero[la_menor_pos])
                la_menor_pos = cual;

        intercambia (&numero[menor], &numero[la_menor_pos]);
    }
}




void ordena_1 (int *numero, int cuantos) {

    for (int menor=0; menor<cuantos-1; menor++)
        for (int cual=menor+1; cual<cuantos; cual++)
            if (numero[cual] < numero[menor])
                intercambia (&numero[menor], &numero[cual]);
}





void
mostrar_tiempo (clock_t lapso, int cuantos)
{
    double tiempo;

    tiempo = (double) (lapso) / CLOCKS_PER_SEC;
    printf ("%i ordenados en %.2lfs.\n", cuantos, tiempo);
}

void
execute (int *numero, int cuantos, void (*psort)(int *, int) )
{
    clock_t begin, end;
    int *desordenado = NULL;

    desordenado = (int *) malloc (cuantos * sizeof (int));
    memcpy(desordenado, numero, cuantos * sizeof (int));
    begin = clock();
    (*psort) (desordenado, cuantos);
    end = clock();
    mostrar_tiempo (end - begin, cuantos);

    free (desordenado);
}




int
main (int argc, char *argv[])
{
    int *numero      = NULL;
    int cuantos;

    /* Inicialización */
    srand (time (NULL));

    /* Entrada de Datos */
    printf ("¿Cuántos números quieres que tenga el vector? ");
    scanf (" %i", &cuantos);

    /* Asignar valores iniciales */
    numero      = (int *) malloc (cuantos * sizeof (int));

    for (int i=0; i<cuantos; i++)
        numero[i] = random () % cuantos + 1;


    // execute (numero, cuantos, &ordena_1);
    // execute (numero, cuantos, &ordena_2);
    // execute (numero, cuantos, &ordena_3);

    void (*pfunc[])(int *, int) = {
        &ordena_1,
        &ordena_2,
        &ordena_3
    };

    for (int i=0; i<3; i++)
        execute (numero, cuantos, pfunc[i]);


#ifndef NDBG
    imprimir (numero, cuantos, NULL);
#endif

    // memcpy(desordenado, numero, cuantos * sizeof (int));
    // begin = clock();
    // ordena_4(desordenado, 0, cuantos-1);
    // end = clock();
    // mostrar_tiempo (end - begin, cuantos);

#ifndef NDBG
    imprimir (desordenado, cuantos, NULL);
#endif


    free (numero);

    return EXIT_SUCCESS;
}
