#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    char *nombre;
    unsigned numero, binario = 0;

    system ("clear");
    system ("toilet -f pagga --gay BINARIO");

    printf ("\n\n");
    printf ("Amadísimo usuario, ¿puedo conocer su nombre?\n");
    printf ("Nombre: ");
    scanf (" %ms", &nombre);

    printf ("%s espero que estés pasando un buen día.\n", nombre);
    free (nombre);

    printf ("Te voy a pasar un número a binario. A ver si acierto.\n");
    printf ("Número: ");
    scanf (" %u", &numero);

    for (int pot=1; numero>0; numero>>=1, pot*=10)
        binario += pot * (numero % 2);

    printf ("%i", binario);
    printf ("\n");

    return EXIT_SUCCESS;
}
