# Problemas de Estructuras de Control

1. Explica la siguiente línea: `if(abs(x)<xmin)x=(x>O)?xmin:-xmin;`. Usa ejemplos con números positivos y negativos.
1. Escribir un bucle que calcule la suma de cada tercer entero, comenzando por i = 2 (es decir,calcular la suma de 2+5+8+11+...) para todos los valores de i menores que 10O. Escribir el bucle de tres formas diferentes:while, do-while, for.
1. Repetir el problema anterior empezando por el número que quiera el usuario y sumando 100 números, en vez de los menores que 100.
1. Escribir un bucle que examine cada carácter en un array de caracteres llamado texto y escriba el equivalente ASCII  (el valor numérico) de cada carácter. Supón que sabes cuántos caracteres hay. for
1. Repite el ejercicio anterior sin saber cuántos caracteres hay. Usa strlen. for
1. Repite el ejercicio anterior sin saber cuántos caracteres hay. Usa el '\0'. while.
1. Repite el ejercicio anterior. Para si te encuentras un asterisco.
1. Empezando por 2, vete generando números de 3 en 3 mientras sean menores que 100. Calcula la suma de aquellos que sean divisibles por 5. En la primera versión usa `?:` y en la otra el `if`.
1. Escribir un bucle que examine cada carácter en un array de caracteres llamado texto y cuente cuantos de ellos son letras, cuantos números y el total de los mismos.
1. Haz un `switch` que examine la varible de tipo caracter `color` cuyos valores pueden ser `'r'`, `'g'`, `'b'` en mayúsculas o minúsculas. Mira la función `tolower`.
1. Pregúntale al usuario por la temperatura y di, usando un `switch` si el agua es Hielo, Líquido o Vapor. (Nota irrelevante: el agua también puede ser gas, pero no viene al caso).
1. Escribir un `for` que leyendo del array de caracteres texto, escriba en otro el texto al revés. Ojo con el `\n` que siempre va al final.
1. Repetir el programa anterior pero ahora el array de origen y destino han de ser el mismo. Es decir sólo hay un array, y éste debe quedar invertido. (Nota irrelevante: las palabras que se pueden leer igual al derecho y al revés se llaman palíndromos. La mas famosa es: dábale arroz a la zorra el abad.)
1. Dada una cadena de texto en mayúsculas, escribirla en pantalla en minúsculas. No uses ninguna función de conversión de la librería estándar. Desarróllalas tú.
1. Repetir el ejercicio anterior, almacenándolo en un array.
1. Calcular la media de diez números.
1. Calcular la media de todos los números positivos que introduzca el usuario. El programa deja de preguntar si el usuario introduce un número negativo.




