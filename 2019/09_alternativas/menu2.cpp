#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    char operador;
    int op1, op2;
    int resultado;
    int op_buena = 0;

    /* Bloque de Entrada de Datos */
    printf ("(s)uma o (r)esta: ");
    scanf (" %c", &operador);

    printf ("Operando: ");
    scanf (" %i", &op1);

    printf ("Operando: ");
    scanf (" %i", &op2);

    /* Calculo */
    if (operador == 's'){
        resultado = op1 + op2;
        op_buena = 1;
    }

    if (operador == 'r'){
        resultado = op1 - op2;
        op_buena = 1;
    }


    if ( !op_buena ) {
        fprintf (stderr, "Eres un pringao.\n");
        return EXIT_FAILURE;
    }

    /* Salida de Datos */
    printf ("%i %c %i = %i\n",
            op1,
            operador == 's'? '+' : '-',
            op2,
            resultado);

    return EXIT_SUCCESS;
}
