#ifndef __STACK_H__
#define __STACK_H__

#define M 0x05

struct TStack {
    int data[M];
    int summit;
    int failed;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void init (struct TStack *s);
    void push (struct TStack *s, int ndata);
    int pop (struct TStack *s);
#ifdef __cplusplus
}
#endif

#endif
