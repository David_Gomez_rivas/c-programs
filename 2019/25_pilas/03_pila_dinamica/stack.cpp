#include "stack.h"

#include <stdlib.h>

#define BLCKSZ 5U

void allocate (struct TStack *s) {
    s->data = (int *) realloc (s->data,
            (s->size + BLCKSZ) * sizeof(int) );
    s->size += BLCKSZ;

}

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TStack *s) {
    s->data   = NULL;
    s->summit = 0;
    s->failed = 0;
    s->size   = 0;
    allocate (s);
}

void destroy (const struct TStack *s) {
    free (s->data);
}

void push (struct TStack *s, int ndata) {
    s->failed = 0;
    while (s->summit >= s->size)
        allocate (s);

    s->data[s->summit++] = ndata;
}

int pop (struct TStack *s) {
    s->failed = 0;
    if (s->summit <= 0) {
        s->failed = 1;
        return -666;
    }
    return s->data[--s->summit];
}
