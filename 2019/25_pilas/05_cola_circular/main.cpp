#include <stdlib.h>
#include <time.h>

#include "queue.h"
#include "interface.h"

void show_data (const struct TQueue *queue) {
    show_queue (*queue);
    printf ("Pulse una tecla para continuar.");
    getchar ();
}

void fill (struct TQueue *q, unsigned s) {
    for (unsigned i=0; i<s; i++) {
        push (q, rand () % 10 - 3);
        if (q->failed) {
            show_error ("Stack push operation failed.");
            break;
        }
    }

    show_data (q);
}

int extract (struct TQueue *q) {
    int yield = shift (q);

    if (q->failed)
        show_error ("Stack pop operation failed.");

    show_data (q);

    return yield;
}

int main (int argc, char *argv[]) {
    struct TQueue queue;

    init (&queue);

    srand (time (NULL));
    fill (&queue, 4);

    for (int i=0; i<3; i++)
        printf ("\t=> %i\n", extract (&queue));

    fill (&queue, 3);

    return EXIT_SUCCESS;
}
