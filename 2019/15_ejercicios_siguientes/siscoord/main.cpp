#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "interfaz.h"
#include "euclides.h"

int main (int argc, char *argv[]) {

    enum TBase srcbas, dstbas;
    double src_vec[DIM], dst_vec[DIM];

    /* Entrada de Datos */
    menu ( &srcbas, &dstbas );
    title ();
    ask_vector (srcbas, src_vec);

    /* Salida Sin Cálculos */
    if (srcbas == dstbas) {
        print_result (src_vec, src_vec, srcbas, dstbas);
        return EXIT_SUCCESS;
    }

    /* Cálculos */
    switch (srcbas) {
        case cart:
            switch (dstbas) {
                case cil:
                    car2cil (dst_vec, src_vec);
                    break;
                case esf:
                    car2esf (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    return EXIT_FAILURE;
            }

            break;
        case cil:
            switch (dstbas) {
                case cart:
                    cil2car (dst_vec, src_vec);
                    break;
                case esf:
                    cil2esf (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    return EXIT_FAILURE;
            }
            break;
        case esf:
            switch (dstbas) {
                case cart:
                    esf2car (dst_vec, src_vec);
                    break;
                case cil:
                    esf2cil (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    return EXIT_FAILURE;
            }
            break;
        default:
            fprintf (stderr, "Base de entrada desconocida.\n");
            return EXIT_FAILURE;
    }

    /* Salida de Datos */
    print_result (src_vec, dst_vec, srcbas, dstbas);
    printf ("\n");

    return EXIT_SUCCESS;
}

