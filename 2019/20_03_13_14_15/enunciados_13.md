# Enunciados Fin de Semana

Un polinomio tiene la forma: 

```math
3x^3 + 4x^2 - 3
```

Por ejemplo.  A este polinomio le falta el termino en x. También lo podríamos haber puesto como: 


```math
3x^3 + 4x^2 + 0x - 3
```

O,

```math
3x^3 + 4x^2 + 0x^1 - 3x^0
```

Sabiendo que cualquier cosa elevada a 0 es 1.

El grado de este polinomio es la mayor potencia que tiene. En este caso: 3.

Por la propiedad conmutativa podemos escribir:


```math
- 3x^0 + 0x^1 + 4x^2 + 3x^3
```

Que hace que la potencia nos pueda coincidir con el número de celda si lo represantamos en un array

```c
double pol[] = {-3, 0, 4, 3};
```

Hoy se trata de confeccionar algunos programas en torno a los polinomios.

1. Haz un programa con una función 

```c
double f(double *pol, int grado, double x)
```

que dado un valor de x compute cuánto vale el polinomio.

1. Haz un programa que pregunte el límite inferior(li), el límite superior (ls) y el incremento (inc)
y que imprima todos los valores del polinomio entre li y ls, cuando se avanza de inc en inc.

1. Desarrolla la función: 

```c
bool zero(double *pol, int grado, double li, double ls)
```
que te diga si el signo del polinomio en li es igual al signo del polinomio en ls. Es decir,
si los valores del polinomio en li y ls son, respectivamente, 3 y 7: Verdadero, -3 y -9: Verdadero, -3, 9: Falso.

1. Haz un programa que pregunte un polinomio, junto a li y ls. li y ls deben dar signo distinto. Divide el intervalo li, ls por la mitad. Este punto medio será el nuevo li o ls de tal forma que el signo de li y ls siga siendo distinto.
Repite el proceso hasta que la distancia entre li y ls sea menor que 0.01. Este método se llama de Newton-Raphson.
