#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>

const char *lista[] = {
    "paralelo",
    "mosquita",
    "arbol",
    "rododendro",
    NULL
};

void muestra (const char *a[] ) {
    for (int i=0; lista[i]!=NULL; i++)
        printf ("%s ", lista[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int menor;

    muestra (lista);
    /* Ordenación de inserción */

    for (int buscando=0; lista[buscando+1]!=NULL; buscando++) {
        menor = buscando;
        for (int i=buscando+1; lista[i]!=NULL; i++)
            if (strcasecmp(lista[i], lista[menor]) < 0 )
                menor = i;
        if (menor > buscando) {
            const char *aux = lista[buscando];
            lista[buscando] = lista[menor];
            lista[menor] = aux;
        }
    }
    /* Fin de la ordenación */
    muestra (lista);


    return EXIT_SUCCESS;
}
