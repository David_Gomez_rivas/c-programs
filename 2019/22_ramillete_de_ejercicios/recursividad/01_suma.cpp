#include <stdio.h>
#include <stdlib.h>

/* Como éste ya lo habíamos hecho pongo
 * una solución más elegante que junta
 * la condición de salida con la llamada
 * recursiva  */

int suma (int n){
    return n + (n <= 0 ? 0: suma (n-1) );

}

int main (int argc, char *argv[]) {

    const int n = 6;

    printf ("suma (%i) = %i\n", n, suma (n));

    return EXIT_SUCCESS;
}
