# Ejercicios de Estructuras

1. Define la estructura TCordenada con los campos x e y. Define las variables posición y velocidad. Pregúntale al usuario por sus valores. Define un bucle infinito que incremente los campos de posición con los valores de los campos de velocidad a cada vuelta. Imprime también a cada vuelta los valores de posición y velocidad y usa `usleep` para que le dé tiempo al usuario de visualizar lo imprimido.

1. Declara, sin usar arrays, dos empleados, que tienen nombre, apellidos, edad y salario. Pregúntale al usuario quién es el empleado del mes y almacénalo en un puntero. Incrementa en un 1% el salario del empleado del mes. Utiliza ese puntero para imprimir los datos finales del elegido.

