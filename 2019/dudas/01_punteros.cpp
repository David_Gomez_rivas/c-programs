#include <stdio.h>
#include <stdlib.h>

double *pedir (double **n) {
    double buffer;

    printf ("Número: ");
    scanf ("%lf", &buffer);

    /*   n         num
     * +---+      +---+
     * | o-+----->| o-+----->| HEAP |
     * +---+      +---+
     */

    if ( n != NULL ) {
        *n  = (double *) malloc (sizeof (double));
        **n = buffer;
        return NULL;
    }

    double *minum = (double *) malloc (sizeof (double));
    *minum = buffer;

    return minum;
}

int main (int argc, char *argv[]) {

    double *num1 = NULL,
           *num2 = NULL;

    pedir (&num1);
    num2 = pedir (NULL);


    printf ("Numero: %.2lf\n\n", *num1);
    printf ("Numero: %.2lf\n\n", *num2);

    free (num1);
    free (num2);

    return EXIT_SUCCESS;
}
