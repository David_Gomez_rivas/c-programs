#include <stdio.h>
#include <stdlib.h>

enum TFunc {op_suma, op_resta};


int suma (int op1, int op2) { return op1 + op2; }
int rest (int op1, int op2) { return op1 - op2; }

int main (int argc, char *argv[]) {

    int (*p) (int, int);
    int opcion;

    printf ("Suma o resta (0/1): ");
    scanf (" %i", &opcion);

    switch (opcion) {
        case op_suma:
            p = &suma;
            break;
        case op_resta:
            p = &rest;
            break;
     }

    printf ("2 + 3 = %i\n", (*p) (2, 3) );    // 2000 (2,3)

    printf ("\n");

    return EXIT_SUCCESS;
}
