#include <stdio.h>
#include <stdlib.h>


#define M 2
#define N 3

void recibir (int *m, int d) {
    for (int i=0; i<d; i++) {
        printf ("Dato (%i): ", i+1);
        scanf (" %i", m + i);
    }
    printf ("\n");
}

void mostrar (int m[N]) {
    for (int i=0; i<N; i++)
        printf ("%6i", m[i]);
    printf ("\n");
    printf ("\n");
}

void recibir2 (int *m, int f, int c) {
    for (int i=0; i<f; i++)
        for (int j=0; j<c; j++) {
            printf ("Dato (%i, %i): ", i+1, j+1);
            scanf (" %i", m + i*c + j );
        }

}

int main (int argc, char *argv[]) {
    int a[N], b[M][N];

    recibir2 ((int *) b, M, N);
    // recibir ((int *) b, N);
    // recibir (b[1], N);

    mostrar (b[0]);
    mostrar (b[1]);

    return EXIT_SUCCESS;
}
