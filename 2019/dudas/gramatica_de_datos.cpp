#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2) { return op1 + op2; }

int main (int argc, char *argv[]) {

    int (*p) (int, int);

    p = &suma;

    (*p) (2, 3);

    return EXIT_SUCCESS;
}
