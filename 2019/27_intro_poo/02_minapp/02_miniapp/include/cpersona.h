#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x100

/*
 * AMBITOS
 * m_ Variables miembro.
 * g_ Variables globales
 *
 * Uso
 * n  Número
 * d  Diferencias
 * p  Puntero
 * c  Cuenta
 * i  Índice
 * sz Tamaños
 * fn Función
 * f  Bandera
 * rg Array
 * */

class CPersona {
    unsigned edad;
    char nombre[MAX];

    public:

    CPersona () = delete;
    CPersona (unsigned edad, const char *nombre);
    void display ();
};

#endif
