#include "persona.h"

#include <stdio.h>
#include <string.h>

void init (struct TPersona *p, unsigned edad, const char *nombre) {
    p->edad = edad;
    strcpy (p->nombre, nombre);
}

void display (const struct TPersona *p) {
    printf ("Nombre: %s\n", p->nombre);
    printf ("Edad: %u\n",   p->edad);
}
