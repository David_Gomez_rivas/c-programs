#include <stdlib.h>

#include "persona.h"

int main (int argc, char *argv[]) {

    TPersona persona;

    init (&persona, 34, "Pepe");
    display (&persona);

    return EXIT_SUCCESS;
}
