
#ifndef CMAMIFERO_H
#define CMAMIFERO_H

class CMamifero
{
public:

  unsigned get_extremidades () const;
  virtual void anda () = 0;	// Función virtual pura
  // Contrato para las clases derivadas
  // Convierte a CMamifero en una clase abstracta

protected:
    CMamifero ();
    CMamifero (unsigned c_extrem);

private:
  unsigned c_extremidades_;
};

#endif // CMAMIFERO_H
