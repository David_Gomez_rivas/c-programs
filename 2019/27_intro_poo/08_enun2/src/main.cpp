/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Test of monster and men
 *
 *        Version:  1.0
 *        Created:  02/06/20 12:02:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  txemagon / imasen (), txemagon@gmail.com
 *   Organization:  txemagon
 *
 * =====================================================================================
 */

#include <stdlib.h>

#include "CHumano.h"
#include "CEquino.h"

int
main ()
{
    CHumano h(2);
    CEquino e;

    h.saluda ();
    h.anda ();
    e.anda ();

    return EXIT_SUCCESS;
}
