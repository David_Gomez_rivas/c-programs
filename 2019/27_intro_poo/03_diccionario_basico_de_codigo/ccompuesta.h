#ifndef __CCOMPUESTA_H__
#define __CCOMPUESTA_H__

#include "ccompuesto.h"

class CCompuesta {
    CCompuesto *compuesto;
    CCompuesto el2;

    public:
    CCompuesta ();
    CCompuesta (CCompuesto el);
    CCompuesta (const CCompuesta &ref);
    ~CCompuesta ();
};

#endif
