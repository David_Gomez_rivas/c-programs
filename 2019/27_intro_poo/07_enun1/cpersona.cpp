/*
 * =====================================================================================
 *
 *       Filename:  cpersona.cpp
 *
 *    Description: Implementación de la clase persona
 *
 *        Version:  1.0
 *        Created:  02/06/20 09:58:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  txemagon / imasen (), txemagon@gmail.com
 *   Organization:  txemagon
 *
 * =====================================================================================
 */

#include "cpersona.h"
#include <string.h>
#include <iostream>

using namespace std;

/*
 *--------------------------------------------------------------------------------------
 *       Class:  CPersona
 *      Method:  CPersona :: CPersona
 * Description:  Constructor
 *--------------------------------------------------------------------------------------
 */
CPersona::CPersona (const char *nom)
{
    strncpy (this->nombre, nom, MAX);
}


/*
 *--------------------------------------------------------------------------------------
 *       Class:  CPersona
 *      Method:  CPersona :: saluda
 * Description:  Saluda
 *--------------------------------------------------------------------------------------
 */
void
CPersona::saluda () {
    cout << "Hola. Me llamo " << this->nombre << "\n";
}
