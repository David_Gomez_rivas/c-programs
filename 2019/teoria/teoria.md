# Teoría

Programación de las sesiones de teoría.

## Martes 24.

1. Estudia el fichero sobre [tipos de datos](01_tipos_de_datos.md).
1. Ver el vídeo sobre variables estáticas: https://youtu.be/CehQEIxfiKk
1. Estudiar el vídeo sobre recursividad y su [archivo adjunto](02_recursividad.md): https://youtu.be/thcfQjvOmJE
1. Metodo de ordenación de la burbuja: https://youtu.be/LCtM-LxjJRI
1. Metodo de ordenación por inserción: https://youtu.be/cXczPPShJmw

Abril
## Jueves 1

1. Gramática de los tipos de datos: https://youtu.be/wTvBs3sxvqI
