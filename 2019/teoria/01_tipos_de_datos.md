# Tipos de Datos

A continuación presento una tabla resumén con la lógica de cómo el compilador analiza los tipos de datos. No todos
los datos que detallo a continuación tienen por qúe ser posibles, pero si lo fueran se escribirían así. Intenta
descifrar cuál es la lógica que sigue el compilador.

| Tipo de dato              | Explicación                                                                        |
| :------------:            | -------------                                                                      |
| `int *p`                  | p es un puntero a una cantidad entera.                                             |
| `int *p[10]`              | p es un array de 10 punteros a enteros.                                            |
| `int (*p)[10]`            | p es un puntero a un array de 10 enteros[^1].                                      |
| `int *p (void)`           | p es una función que devuelve un puntero a entero.                                 |
| `int p (char *a)`         | p es una función. Acepta un puntero a caracter y devuelve un entero.               |
| `int *p (char *a)`        | p es una función. Acepta un puntero a caracter y devuelve un puntero a entero.     |
| `int (*p) (char *a)`      | p es un puntero a una función. Acepta puntero a char. Devuelve int.                |
| `int (*p (char *a))[10]`  | p es una función que acepta puntero a char. Devuelve puntero a array de 10 int.[^2]|
| `Escríbelo tú`            | Declara un puntero a la función anterior.                                          |
| `int *p (char (*a)[])`    | p es una función. Acepta un puntero a un array de caracters. Devuelve un puntero.  |
| `int p (char *a[])`       | p es una función. Acepta un array de punteros a char. Devuelve un entero.        |
| `int *p (char a[])`       | p es una función. Acepta un array de char. Devuelve un puntero a entero.           |
| `int *p (char (*a)[])`    | p es una función. Acepta un puntero a un array de char. Devuelve puntero a entero. |
| `int (*p) (char (*a)[])`  | p es un puntero a una función que acepta un puntero a un array de char. Dv: int    |
| `int *(*p) (char (*a)[])` | Idem, pero devuelve un puntero a entero la función apuntada.                       |
| `int *(*p) (char *a[])`   | p es un puntero a una función que acepta un array de char. Dv: puntero a int       |
| `int (*p[10]) (void)`     | p es un array de 10 punteros a función sin parámetros que devuelven int.           |
| `int *(*p[10]) (char a)`  | Ídem, pero devuelven puntero a entero.                                             |
| `int (*p) (int (*b)() )`  | Puntero a una función que acepta un puntero a una función y devuelve int.          |

Algunos tipos imposibles se formarían añadiendo [] al final de la declaración de funciones o sus respectivos punteros. Son imposibles porque en c no se pueden devolver arrays:


| Tipo de dato                | Explicación                                                                     |
| :------------:              | -----------                                                                     |
| `int *p (void)[]`           | p es una función que devuelve un array de punteros a entero.                    |
| `int p (char *a)[]`         | p es una función. Acepta un puntero a caracter y devuelve un array de enteros.  |
| `int (*p) (char *a)[]`      | p es un puntero a una función. Acepta puntero a char. Devuelve un array de int. |


[^1]:
```
int (*p)[10]
+---+     +---+     +---+---+---+---+---+---+
| o-+---->| o-+---->|   |   |   |   |   |   |
+---+     +---+     +---+---+---+---+---+---+
  p       Array     Reserva del array.
```

[^2]: Esta es la declaración más difícil desde mi punto de vista.
