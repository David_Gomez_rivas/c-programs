#ifndef __USAGE_H__
#define __USAGE_H__

#include <stdio.h>

extern const char *prog_name;

#ifdef __cplusplus
extern "C"
{
#endif

    void usage_init        (const char *name);
    void print_usage (FILE *stream);

#ifdef __cplusplus
}
#endif

#endif
