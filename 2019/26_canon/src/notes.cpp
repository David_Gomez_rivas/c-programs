#include "notes.h"
#include "scales.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

enum TDuration_k {
    semibreve, /* redonda     */
    minim,     /* blanca      */
    crotchet,  /* negra       */
    quaver,    /* corchea     */
    semiquaver /* semicorchea */
};

unsigned note_fraction [] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 };
const char *duration_symbols[] = {"𝅝", "𝅗𝅥", "𝅘𝅥", "𝅘𝅥𝅮", "𝅘𝅥𝅯", "𝅘𝅥𝅰", "𝅘𝅥𝅲" };
unsigned fig_len = sizeof (duration_symbols) / sizeof (char *);

// Find name index of a name
enum TNoteName look_for_name (const char *name) {
    for (unsigned i=0; i<name_len; i++)
        if (strcasecmp (note_names[i], name) == 0)
            return (enum TNoteName) i;

    /* If we reach this point there is an invalid note name
     * and the score is invalid. */
    fprintf (stderr, "Invalid note name: %s\n\n", name);
    exit (1);
}

struct TNote create_note (unsigned duration, const char note_name[NL], unsigned octave, struct TScale sc) {
    struct TNote note;

    char nn[NL];
    int dotted = isupper (note_name[1]);

    if (duration > sizeof(note_fraction) / sizeof (char *) - 1) {
        fprintf (stderr, "Note %s has a duration of %u. This is too short.\n", note_name, duration);
        exit (1);
    }

    if (dotted && duration == 0) {
        fprintf (stderr, "Semibreve cannot be dotted. \n");
        exit (1);
    }

    strcpy (nn, note_name);               // Dotted notes are ending in capitals.
                                          // We avoid changing note name parameter
                                          // to minimize side effects on caller.
    strcpy (note.read_name, note_name);

    // Note name normalized without capitals.
    for (unsigned i=0; i<NL-1; i++)
        nn[i] = tolower (note_name[i]);

    note.name_index = look_for_name (nn);
    note.tone_id = note_value (sc, note.name_index);

    if (dotted) {
        note.duration.numerator = 3;
        note.duration.denominator = note_fraction[duration+1] ;

    } else {
        note.duration.numerator = 1;
        note.duration.denominator = note_fraction[duration];
    }

    note.octave = octave;

    return note;
}

// Returns printable name for the active scale.
const char *note_name (enum TNoteName index, struct TScale sc) {
    if ( (int) index >= name_len)
        fprintf (stderr, "Invalid index for note name.\n");

    return (const char *) outputnames[sc.mode][(int) sc.nn_idx][(int) index % name_len];
}

// Return figure symbol for a given fraction
const char *note_fig  (unsigned duration) {
    duration = (int) round (log2 (duration) );  // round used to prevent log2 rounding errors.
    if (duration >= fig_len)
        fprintf (stderr, "Invalid duration for note figure.\n");

    return (const char *) duration_symbols[duration % fig_len];
}
