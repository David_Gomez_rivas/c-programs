# Enunciados para el Martes

Para ordenar un array hay distintos métodos. Vamos a usar el método de inserción y el de la burbuja.  
Inserción: https://es.wikipedia.org/wiki/Ordenamiento_por_inserci%C3%B3n  
Burbuja: https://es.wikipedia.org/wiki/Ordenamiento_de_burbuja  

1. Preguntar números positivos al usuario. Calcular la mediana. La mediana es aquél número que divide a la población (el conjunto de números) por la mitad. Básicamente el elemento del medio de un array cuando éste está ordenado. El programa dejará de pedir números cuando el usuario introduzca un número negativo. En la versión uno usa el método de inserción y en la dos, el de la burbuja.
