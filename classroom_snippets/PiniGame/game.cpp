#include "game.h"

Game::Game(QWidget *parent)
{
    setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
    setFixedSize (1025, 768);

    scene = new QGraphicsScene (this);
    scene->setSceneRect (0, 0, 1024, 768);
    setScene (scene);

    start();

    beat = new QTimer (this);
    connect (beat, SIGNAL(timeout()), this, SLOT(run()));
    beat->start (BEAT_STEP);
}

int Game::start ()
{
    // Setup
    Nave *nuevo = new Nave();
    moviles << nuevo;
    scene->addItem(nuevo);

    return (int) EXIT_OK;
}

void Game::run ()
{

    for (size_t i=0, n=moviles.size(); i<n; i++)
    {
        moviles[i]->update ();
    }

    for (size_t i=0, n=moviles.size(); i<n; i++)
    {
        moviles[i]->draw ();
    }

}
