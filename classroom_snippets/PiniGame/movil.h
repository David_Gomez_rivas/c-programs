#ifndef MOVIL_H
#define MOVIL_H

#include <QPointF>

struct Coord
{
    QPointF x;
    QPointF y;
};

class Movil
{
public:
    Movil();

    Coord pos, vel, acel;

    void virtual update() {}
    void virtual draw() {}
};

#endif // MOVIL_H
