#include <stdio.h>
#include <stdlib.h>

#define MAX 10
#define INC 0.0001

double polinomio(double c[MAX], double x) {
    double f = 0,
           potencia;

        for (int termino=0; termino<MAX; termino++){
            potencia = 1;
            for (int exponente=0; exponente<termino; exponente++)
                potencia *= x;
            f += c[termino] * potencia;
        }
        return f;
}

/* Función punto de entrada */
int  main(){
    double f = 0, x, ultimo=0;
    double t;
    double c[MAX] = {-10, -2, 3, -4, 5, 0, 0, 0, 0, 0 };

    printf ("Limite Inferior: ");
    scanf (" %lf", &x);

    /*
    1·x^0 + 7·x^1 + 3·x^2 + 4·x^3 + 5·x^4
    c[0]·x^0 + c[1]·x^1 + c[2]·x^2 + c[3]·x^3 + c[4]·x^4
    c[i]·x^i
    f += c[i]·x^i
*/
    for (t=x; t == x || ultimo*f>0 ;t+=INC){
        ultimo = f;
        f = polinomio(c, t);
        if (t == x)
            ultimo = f;
    }

    printf ("Signo cambia en %.4lf\n", t);

    return EXIT_SUCCESS;
}
