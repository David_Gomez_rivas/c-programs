#include <stdio.h>
#include <stdlib.h>

#define N 10

/* Función punto de entrada */
int  main(){

    double  nota[N],
            media = 0;

    /* Entrada de datos */
    for (int i=0; i<N; i++){
        printf ("Nota: ");
        scanf (" %lf", &nota[i]);
    }

    /* Calculos */
    for (int i=0; i<N; i++)
        media += nota[i];

    media /= N;

    /* Salida de Datos */
    printf("Media: %.2lf\n", media);

    return EXIT_SUCCESS;
}
