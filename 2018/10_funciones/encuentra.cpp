#include <stdio.h>
#include <stdlib.h>

int encuentra(int buscado, int A[], int n_elem){
    int pos = -1;

    /* Busca el 13 en A */
    for (int i=0; i<n_elem; i++ )
        if ( A[i] == buscado )
            pos = i;

    return pos;
}

/* Función punto de entrada */
int  main(){
    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};

    int pos = encuentra (3, A, sizeof(A)/sizeof(int));
    if (pos >0)
        printf ("%i está en %i.\n", 3, pos);
    else
        printf ("Nota!.\n");

    return EXIT_SUCCESS;
}
