#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int num){ /* Parámetro formal */
    char titulo[MAX];
    sprintf(titulo, "toilet -fpagga --metal Tabla del %i", num);
    system(titulo);
}

/* Función punto de entrada */
int  main(){
    /* Declaración de variables */
    int tabla, op1 = 1;

    /* Menú */
    printf("¿Qué tabla quieres?\n");
    printf("Tabla: ");
    scanf(" %i", &tabla);

    pon_titulo(3);
    pon_titulo(tabla); /* Llamada con parámetro actual 5 */
    pon_titulo(2*tabla);

    /* Resultados */
    printf("%ix%i=%i\n",op1, tabla, op1 * tabla);
    op1++;
    printf("%ix%i=%i\n",op1, tabla, op1 * tabla);

    return EXIT_SUCCESS;
}
