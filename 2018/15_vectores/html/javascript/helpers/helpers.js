function sanitize(str) {
    return str.chomp().replace(/[^a-zA-Z0-9_\s]/g, "")
}

function add(q, id) {
    id = document.getElementById(id)
    val = Number(id.value)
    val += q
    id.value = val
    id.dispatchEvent(new Event("change"))
}

function round_to(numdec, value) {
    var pow = Math.pow(10, numdec)
    value *= pow
    value = parseInt(value)
    return value / pow
}

function wrap(label, text, id, nl) {
    var str = "<" + label

    if (typeof(id) == "undefined")
        str += " id='" + sanitize(text.underscore()) + "'"
    else if (id)
        str += " id='" + sanitize(id.underscore()) + "'"

    str += ">" + text + "</" + label + ">"
    if (nl)
        str += "<br/>"
    str += "\n"
    return str
}

function property(obj, key) {
    return typeof(obj[key]) == "undefined" || obj[key]
}


function create_node(type, text, attr, par) {
    var n = document.createElement(type)
    var t = document.createTextNode(text)
    n.appendChild(t)
    if (attr)
        for (var i in attr)
            n.setAttribute(i, attr[i])
    if (par)
        par.appendChild(n)
    return n
}

function html_node(html) {
    var node = html
    if (typeof(html) == "string" || html instanceof String)
        node = document.getElementById(html)
    if (!node || !(node instanceof HTMLElement))
        throw "html_node error looking for (" + html + ")."
    return node
}

function prune_dom(container) {
    while (container.firstChild)
        container.removeChild(container.firstChild)
}