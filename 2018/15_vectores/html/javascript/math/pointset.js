function PointSet(p) {
   
    this.id = PointSet.branding()
    this.p = [] /* List of points */
    if (typeof(p) !== "undefined")
        this.p = p
    this.d = [] /* Displacement between points */
    this.s = [] /* Space between points */
    this.S = [] /* Accumulated Space between points */
    this.u = [] /* Unit displacement vector */
    this.digest()
}

PointSet.id = 0
PointSet.branding = function() {
    return PointSet.id++
}

/* digest a list of points and store results */
PointSet.prototype.digest = function() {
    this.S[0] = 0
    for (var i = 0; i < this.p.length - 1; i++) {
        this.d[i] = new Vector(this.p[i + 1][0] - this.p[i][0],
            this.p[i + 1][1] - this.p[i][1])
        this.s[i] = module(this.d[i])

        if (isNaN(this.s[i]))
            this.s[i] = 0

        this.S[i + 1] = this.S[i] + this.s[i]
        this.u[i] = new Vector(this.d[i][0] / this.s[i], this.d[i][1] / this.s[i])
    }
}

/*
 * Find where s (searched cumulative value) is reached starting at i0 on p list.
 */
PointSet.index = function(p, s, i0) {
        i0 = i0 || 0
        for (var i = i0; i < p.length; i++)
            if (p[i] > s)
                return i
    }
    /*
     * p: Points of the curve
     * t: tangents
     * n: normals
     * k: curvature
     * KD: Accumulated deviation due to curvature. Not promediated by distance
     * r: 1 / c => Curvature radio
     * e: Evolute
     */