function Bezier(org, end, sf, anticurl) {
    this.stress = []
    this.anticurl = anticurl || true
    
    /* stress factor for soothing the curve */
    sf = sf || 0.2
    var distance = Vector.subs( end.end, org.org).module()
    org.end = org.org.add(Vector.mul(sf * distance, org.dir))
    end.org = end.end.subs(Vector.mul(sf * distance, end.dir))
    /* I miss here an integral part to take into accoun the history of the tangent */

    this.stress[0] = org
    if (!org.intersects(end))
        this.stress.push(new Segment(org.end, end.org))
    
    if (this.anticurl) {
        var inter = Segment.intersection(org, end)
        if (inter[1] < 0){ /*anticurl prevention because of s shape due to curvature change */
                var i = org.intersect(end)
                this.stress[0] = new Segment(org.org, i)
                end = new Segment(i, end.end)
            }
    }
    
    this.stress.push(end)
}


Bezier.prototype.eval = function(t) {
    var c = []
    for (var i = 0; i < this.stress.length; i++)
        c.push(new Segment(this.stress[i].org, this.stress[i].end))

    while (c.length > 1) {
        for (var i = 0; i < c.length - 1; i++)
            c[i] = new Segment(c[i].eval(t), c[i + 1].eval(t))
        c.pop()
    }

    return c[0].eval(t)
}

Bezier.prototype.trace = function(step) {
    var trail = []
    for (var i = 0; i < 1; i += step)
        trail.push(this.eval(i))

    return trail
}