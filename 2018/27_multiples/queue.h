#ifndef  __QUEUE_H__
#define __QUEUE_H__

#include "general.h"

#define MAX 0x04


enum { no, lleno, vacio, ERRORES };
extern int error;

struct TCola {
    int data[MAX];
    int base;
    int cima;
};


#ifdef __cplusplus
extern "C" {
#endif

void push (struct TCola *cola, int dato);
int shift (struct TCola *cola);
const char *vacia (int error);
const char *llena (int error);

#ifdef __cplusplus
}
#endif

#endif
