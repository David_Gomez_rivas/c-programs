#ifndef GTKMM_EXAMPLE_WINDOW_H
#define GTKMM_EXAMPLE_WINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/separator.h>
#include <gtkmm/button.h>
#include <gtkmm/radiobutton.h>

class ExampleWindow : public Gtk::Window
{
    public:
        ExampleWindow ();
        virtual ~ExampleWindow ();

    protected:
        // Signal handlers
        void on_button_clicked ();

        // Child widgets
        Gtk::Box m_box_top, m_box_1, m_box_2;
        Gtk::RadioButton m_rb1, m_rb2, m_rb3;
        Gtk::Separator m_separator;
        Gtk::Button m_button_close;
};


#endif  //GTKMM_EXAMPLE_WINDOW_H
