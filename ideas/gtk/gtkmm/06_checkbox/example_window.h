#ifndef GTKMM_WINDOW_H
#define GTKMM_WINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/checkbutton.h>


class ExampleWindow : public Gtk::Window
{
    public:
        ExampleWindow ();
        virtual ~ExampleWindow ();

    protected:
        // Signal handlers
        void on_button_clicked ();

        // Child widgets
        Gtk::CheckButton m_button;
};



#endif //GTKMM_WINDOW_H
