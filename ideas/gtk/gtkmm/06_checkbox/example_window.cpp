#include "example_window.h"
#include <iostream>


ExampleWindow::ExampleWindow ()
    : m_button("something")
{
    set_title("Checkbutton Example");
    set_border_width (10);

    // Connect signals
    m_button.signal_clicked().connect(sigc::mem_fun(*this,
                &ExampleWindow::on_button_clicked));

    add (m_button);
    show_all_children ();
}



ExampleWindow::~ExampleWindow ()
{

}


void
ExampleWindow::on_button_clicked ()
{
    std::cout << "The button was clicked: state="
        << (m_button.get_active () ? "true" : "false")
        << std::endl;
}
