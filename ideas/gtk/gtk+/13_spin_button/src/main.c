#include <gtk/gtk.h>



typedef struct {
  GtkWidget *w_lbl_quantity;
  GtkWidget *w_sbtn_quantity;
} app_widgets;




int main(int argc, char *argv[])
{
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new (app_widgets);

    gtk_init(&argc, &argv);

    // builder = gtk_builder_new();
    // gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);
    // Update October 2019: The line below replaces the 2 lines above
    builder = gtk_builder_new_from_file("glade/window_main.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    widgets->w_lbl_quantity  = GTK_WIDGET (gtk_builder_get_object (builder, "lbl_quantity"));
    widgets->w_sbtn_quantity = GTK_WIDGET (gtk_builder_get_object (builder, "sbn_quantity"));
    gtk_builder_connect_signals(builder, widgets);

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
  
    g_slice_free (app_widgets, widgets);

    return 0;
}

void on_btn_update_clicked (GtkButton *button, app_widgets *widgets)
{
  guint32 quantity = 0;
  gchar out_str[100] = { 0 };
  
  quantity = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (widgets->w_sbtn_quantity));
  g_snprintf (out_str, sizeof(out_str), "%i", quantity);
  gtk_label_set_text (GTK_LABEL (widgets->w_lbl_quantity), out_str);
}

// called when window is closed
void on_window_main_destroy()
{
    gtk_main_quit();
}
