/*
 * =====================================================================================
 *
 *       Filename:  exceptions.cpp 1.0 04/06/20 18:41:00
 *
 *    Description:  Some notes on exceptions
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <iostream>
//#include <stdexcept>

using namespace std;
int
main () {
    int dividend, divisor, quotient;

    try
    {
        cout << "Dividend: ";
        cin >> dividend;
        cout << '\n';

        cout << "Divisor: ";
        cin >> divisor;
        cout << '\n';

        if (divisor == 0)
            throw 0;

        quotient = dividend / divisor;

        cout << "Quotient: " << quotient << '\n';
    }
    catch (int)
    {
        cout << "Division by 0.\n";
    }

    return 0;
}

