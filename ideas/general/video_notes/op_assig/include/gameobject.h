#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include <sprite.h>

class GameObject {

    Sprite *m_s;

  public:
    GameObject ();
    GameObject& operator= (const GameObject& other);
    GameObject& operator= (GameObject other);
};

#endif
