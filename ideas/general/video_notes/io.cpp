/*
 * =====================================================================================
 *
 *       Filename:   1.0 03/06/20 17:23:05
 *
 *    Description:  Video Notes for C++ IO
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 *
 * =====================================================================================
 */

#include <iostream>
#include <iomanip>
#include <fstream>

#define SOURCE "fichero.txt"

using namespace std;

int
main () {
    char var_char;
    char ch_exp;
    int  int_exp;
    string str_var;


    /*-----------------------------------------------------------------------------
     *  Extra IO Operations
     *-----------------------------------------------------------------------------*/
    cin.get (var_char);             // Get a single char (useful for whitespaces)
    cin.ignore (int_exp, ch_exp);   // ignore int_exp bytes from imput or until ch_exp

    cin.putback (var_char);         // Return a char to the stream
    var_char = cin.peek ();         // Peep buffer
    cin.clear ();                   // Reset from bad reading.


    /*-----------------------------------------------------------------------------
     *  IO Manipulators
     *-----------------------------------------------------------------------------*/
    cout << setprecision (int_exp); // Decimal amount
    cout << ios::fixed;             // Fixed point decimals
    cout << showpoint;              // Show decimal point on integers
    cout << setw (int_exp);         // Field width
    cout << setfill (ch_exp);       // Padd with a character
    cout << left;                   // Align left
    cout.unsetf ( ios::left );      // Unset alignment
    cout << right;
    getline (cin, str_var);         // Read whole line including spaces


    /*-----------------------------------------------------------------------------
     *  File Input Output
     *-----------------------------------------------------------------------------*/

    ifstream in_data;
    ofstream out_data;

    in_data.open (SOURCE);
    out_data.open (SOURCE);

    /* File operations*/

    in_data.close ();
    out_data.close ();
}
