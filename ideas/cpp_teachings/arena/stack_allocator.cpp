#include <iostream>
#include <cstdint>
#include <cstddef>
#include <cstdlib>
#include <cassert>



static constexpr uint64_t FIRST_ADDR = 24;
static           size_t   memoryUsed =  0;

struct AllocMetaInfo {
    uint64_t adjustment;
};

static void *alignForward(const void* address, uint8_t alignment)
{
    return (void *) (
            (reinterpret_cast<uintptr_t>(address) +
             static_cast<uintptr_t>(alignment - 1)) &
        static_cast<uintptr_t>(~(alignment - 1)));
}

static uint8_t getAdjustment(const void* address, uint8_t alignment)
{
   uint8_t adjustment = alignment - (reinterpret_cast<uintptr_t>(address) & static_cast<uintptr_t>(alignment - 1));

   return adjustment == alignment ? 0 : adjustment;
}

static uint8_t getAdjustment(const void* address, uint8_t alignment, uint8_t extra)
{
    uint8_t adjustment = getAdjustment(address, alignment);

    uint8_t neededSpace = extra;

    if (adjustment < neededSpace)
    {
        neededSpace -= adjustment;
        adjustment  += alignment * (neededSpace / alignment);

        if (neededSpace % alignment > 0)
            adjustment += alignment;
    }

    return adjustment;
}

void *allocate(size_t memSize, uint8_t alignment)
{
    assert(memSize > 0 && "allocate called with memSize = 0.");

    std::cout << "\n\n" << "ALLOCATE:\n";
    std::cout <<"----------------------------------------\n";
    std::cout << "Alignment:\t" << (unsigned) alignment << "\n";
    std::cout << "Mem Size:\t"  << memSize   << "\n";
    union {
        void           *asVoidPtr;
        uintptr_t       asUptr;
        AllocMetaInfo  *asMeta;
    };

    std::cout << "First Address:\t" << FIRST_ADDR << "\n";
    asVoidPtr = (void*) FIRST_ADDR;
    std::cout << "Memory Used:\t" << memoryUsed << "\n";
    asUptr   += memoryUsed;
    std::cout << "Free Address:\t" << asUptr << "\n";

    uint8_t adjustment = getAdjustment(asVoidPtr, alignment, sizeof(AllocMetaInfo));

    //asMeta->adjustment = adjustment;
    std::cout << "Adjustment: \t" << asUptr;
    printf(" [%u]\n", adjustment);

    asUptr += adjustment;
    memoryUsed += memSize + adjustment;
    std::cout << "Memory Used:\t" << memoryUsed << "\n";

    std::cout << "VoidPtr: \t" << asVoidPtr << "\n";
    std::cout << "asUptr:  \t"  << asUptr    << "\n";
    std::cout << "\n";

    return asVoidPtr;

}

int
main() {
    system ("clear");
    for (unsigned i=0; i<4; i++)
        allocate(10, 16);

    return EXIT_SUCCESS;
}
