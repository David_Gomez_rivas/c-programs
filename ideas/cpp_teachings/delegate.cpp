#include <iostream>

class CTestClass {
    public:
        CTestClass() { std::cout << "Constructing CTestClass" << std::endl; }
        void method (int n) { std::cout << "Method called with value (" << n << "): " << std::endl; }
        ~CTestClass() { std::cout << "Destructing CTestClass" << std::endl; };
};

float afunction(int n, float f) {
    std::cout << "Function called with values (" << n << ", " << f << ")" << std::endl;
    return f*n;
}

int main(void) {
    CTestClass o;
    float (*pfunc)(int, float)    = &afunction;
    void (CTestClass::*pmet)(int) = &CTestClass::method;

    (*pfunc)(5, 7.6);

    (o.*pmet)(7);

    return 0;
}
