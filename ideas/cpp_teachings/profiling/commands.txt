sudo apt install oprofile

sudo operf -g --event=CPU_CLK_UNHALTED:2080000 <programa>

# -g: Depurar todo el stack de llamadas
# CPU_CLK_UNHALTED: Depurar todas las instrucciones que no sean HALT
# 2080000 Frecuencia de sampleo.

opreport --symbols

# --symbols: Etiquetas de las funciones

