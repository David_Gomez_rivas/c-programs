#include <iostream>

// RAII: Resource acquisition is initialitation



class CGameObject {
    public:
        CGameObject(const std::string &name)
            : m_name (name)
        {
            std::cout << "Construction of " << m_name << std::endl;
        }

        ~CGameObject() {
            std::cout << "Destruction of " << m_name << std::endl;
        }

        CGameObject& operator=(const CGameObject& g2) {

        }

    private:
        std::string m_name;
};


void input  ()           { std::cout << "input\n";  }
void render ()           { std::cout << "render\n"; };
void update (bool &p) {
    std::cout << "update\n";
    throw std::runtime_error ("Runtime exception");
    p = false;
};

class CGameResource {
    public:
    CGameResource(const std::string& name)
        : m_obj(nullptr)
    {
        init(name);
    }

    ~CGameResource() {
        if (m_obj) {
            delete m_obj;
            m_obj = nullptr;
        }
    }

    void init(const std::string& name) {
        m_obj = new CGameObject(name);
    }

    CGameObject *p() { return m_obj; }

    private:
    CGameObject *m_obj;

};


void rungame() {
    bool playing = true;
    //CGameObject* g = new CGameObject("miguel");
    CGameResource g("Miguel");

    while (playing) {
        input();
        update(playing);
        render();
    }

    //delete g;
}


int main () {
    try {
        rungame();
    } catch (std::runtime_error e) {
        std::cout << e.what() << ". Abort.\n";
    }
}
