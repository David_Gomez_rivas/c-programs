#include <iostream>
#include <cstdint>

const uint32_t size = 10000000;

uint32_t values[size] = {1};
volatile uint32_t m_total = 0; // Puede ser accedida externamente.

void sum() {
    for (uint32_t i=0; i<size; ++i) {
        m_total += values[i];
    }
}

void sum2() {
    uint32_t aux = m_total;
    for (uint32_t i=0; i<size; ++i) {
        aux += values[i];
    }

    m_total = aux;
}

int main() {
    sum2();
    std::cout << m_total << "\n";

    return 0;
}
