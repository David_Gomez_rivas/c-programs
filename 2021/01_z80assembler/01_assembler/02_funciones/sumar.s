_sumar::
    pop   de      ; Obtener la dirección de retorno
    pop   bc      ; Obtener los dos parámetros de 8 bits

    ld    a, b    ; Llevar al acumulador el primer parámetro
    add   c       ; Sumar el segundo parámetro
    ld    l, a    ; Llevar el resultado para retornarlo

    push  bc      ; Equilibrar la pila
    push  de      ; Introducir la dirección de retorno para ret

    ret

;; Convención Devolver
;;   8 bits:  l
;;  16 bits: hl
;;  32 bits: de hl
