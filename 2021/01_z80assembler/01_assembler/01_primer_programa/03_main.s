;; VARIABLES

; Al poner una etiqueta puedo acceder a esta dir de mem.
x: .db 0x88

;; Constantes

vmem  = 0xC000



_main::
        ld    a, (x)
        ld    (vmem), a
	jr    .					; La dirección de mem antes de traducir esta línea


;;asz80 -l -o -s main.s
