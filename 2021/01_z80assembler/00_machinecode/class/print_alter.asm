PRINT_CHAR	equ	&BB5A
YES		equ	'Y'
NO		equ	'N'


;;;;;;;;;;;;;;;;;;;;;
;; ALTERNATIVE DEMO
;;;;;;;;;;;;;;;;;;;;;
org &8000

	ld	a,(data)

substract:
	sub	2
	jr	z, set_even
	jr	c, set_odd
	jr	substract


set_even:
	ld	a,YES
	jr	end_condition

set_odd:
	ld	a,NO

end_condition:
	call	PRINT_CHAR


	ret

data:

	db	08

