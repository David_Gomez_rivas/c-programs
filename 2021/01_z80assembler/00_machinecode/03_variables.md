Programa que hace:

```
C000  FF
C001  81
C002  FE
C003  80
C004  FD
C005  7F
C006  FC
C007  7E
```

```assembly
4000  3E FF         LD  A,#FF
4002  06 81         LD  B,#81
4004  0E 06         LD  C,#06
4006  21 00 C0      LD  HL,#C000
4009  77            LD  (HL),A
400A  3D            DEC A
400B  23            INC HL
400C  70            LD  (HL),B
400D  05            DEC B
400E  23            INC HL
400F  0D            DEC C
4010  20 F7         JR  NZ,#4009
4012  18 FE         JR  #4012
```

Pero si tenemos más patrones nos quedamos sin registros.

```
C000  FF
C001  81
C002  49
C003  A5
C004  FE
C005  80
C006  48
C007  A4
```


Podemos empezar el programa después, dejando así espacio.

```
4000  FF
4001  81
4002  49
4003  A5
```

Parte clave del programa:

```assembly
3A 00 40          LD  A,(4000)
77                LD  (HL),A
3D                DEC A
32 00 40          LD  (4000),A
```


Resumen de instrucciones:


```assembly
32 FF EE      LD (EEFF),A
22 FF EE      LD (EEFF),HL
2A FF EE      LD HL,(EEFF)
3A FF EE      LD A,(EEFF)
77            LD (HL),A
7E            LD A,(HL)
```

Desensambla el siguiente programa:


```
FE 81 21 00 C0 06 06 3A 00 40 77 3D 32 00 40 23 3A 01 40 77 3D 32 01 40 23 05 20 EB 18 FE
```
