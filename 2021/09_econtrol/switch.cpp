#include <stdio.h>
#include <stdlib.h>

#define SUMAR 1
#define RESTAR 2

enum TOpcion {sumar=1, restar};

int menu () {
    printf ("1. Sumar.\n");
    printf ("2. Restar.\n");

    return 2;
}

int main (int argc, char *argv[]) {

    int opcion = menu ();

    switch (opcion) {
        case SUMAR:
            printf ("Sumar.\n");
            break;
        case RESTAR:
            printf ("Restar.\n");
            break;
        default:
            printf ("Opción no válida.\n");
    }

    // Variable de tipo entero que solo tiene permitidos los valores 1 y 2.
    enum TOpcion mi_opcion;
    mi_opcion = sumar;


    return EXIT_SUCCESS;
}
