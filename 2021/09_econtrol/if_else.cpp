#include <stdio.h>
#include <stdlib.h>

// Predicado
int es_multiplo_de (int valor, int multi) {
    return valor % multi == 0;
}

int main (int argc, char *argv[]) {

    int numero;

    printf ("Número: ");
    scanf ("%i", &numero);

    if (numero == 3)
        printf ("Has puesto 3.\n");

    if (numero % 2 == 0)
        printf ("El número %i es par.\n", numero);
    else
        printf ("El número %i es impar.\n", numero);

    if (numero) // numero != 0
        printf ("%i es distinto de 0.\n", numero);

    if (!numero) // numero == 0
        printf ("%i es igual de 0.\n", numero);

    if (es_multiplo_de(numero, 3))
        printf ("%i es multiplo de 3.\n", numero);

    // Operadores relacionales y lógicos.
    if (2<3 && 3<4 )
        printf ("Verdadero.\n");
    return EXIT_SUCCESS;
}
