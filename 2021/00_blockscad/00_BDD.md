BDD

Given	A certain height and a certain length
When    I click on the render button
Then	I should see a Portugese castle

Sketch The Software: UML

TDD

Scenario "Create One wall"
I can create a wall
And the wall must be wider than a cube.
And the height must be a parameter.
And I want a tapered wall.


Scenario "Create multiple walls"

I want a second wall.
Perpendicular to the first one.
