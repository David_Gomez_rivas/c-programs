.include "cpctelera.h.s"


.area _DATA
.area _CODE

.globl cpct_disableFirmware_asm


_main::
   ;; Disable firmware to prevent it from interfering with string drawing
   call cpct_disableFirmware_asm

   ld    hl, #0xC000        ; Load HL with video mem address
   ld    (hl), #0xFF        ; Load color pattern in video mem

loop:
   jr    loop