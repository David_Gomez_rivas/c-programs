
;;
;; VIDEO CONSTANTS
;;
PATTERN 	equ	&FF
VIDEOMEM	equ 	&C000
LINE		equ	&800

REPETITIONS	equ	8

org &9C40
	ld	hl,VIDEOMEM		; Start of vmem
	ld	de,LINE			; Screen line length
	ld	b, REPETITIONS		; Number of lines to be painted

paint:
	ld	(hl),PATTERN		; Memory painted
	add 	hl,de			; Next line
	djnz	paint			; Loop


	ret
