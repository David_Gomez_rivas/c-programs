PATTERN 	equ	&FF
VIDEOMEM	equ 	&C000
LINE		equ	&800

org &8000
	ld	hl,VIDEOMEM
	ld	(hl),PATTERN

	ld	hl,VIDEOMEM+LINE	; This works
	ld	(hl),PATTERN

	ld	hl,(VIDEOMEM+2*LINE)	; This doesn't work
	ld	(hl),pattern

	ld	hl,&D000		; This work
	ld	(hl),pattern

	ret
