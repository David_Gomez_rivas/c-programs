
;;
;; VIDEO CONSTANTS
;;
PATTERN 	equ	&FF
VIDEOMEM	equ 	&C000
SCREENLEN	equ	&4000


org &9C40
	ld	hl,VIDEOMEM		; Start of vmem
	ld	bc,SCREENLEN		; Screen length

paint:
	ld	(hl),PATTERN		; Memory painted
	inc 	hl			; Next pixels
	dec	bc			; Account op
	ld	a,b			; Check if bc is 0
	or	c			
	jr	nz,paint		; Loop


	ret
