#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;



/*********************/
/*   CALCULO DE PI   */
/*********************/

int main() {

     /* DECLARACIÓN DE VARIABLES */
    int op1,      // Operando 1
        op2,      // Operando 2
        result;



    /* ENTRADA DE DATOS */
    printf("Operando 1: ");
    scanf(" %i", &op1);

    printf("Operando 2: ");
    scanf(" %i", &op2);



    /* CÁLCULOS */
    result = op1 + op2;



    /* SALIDA DE DATOS */
    printf("%i + %i = %i\n", op1, op2, result);



    return EXIT_SUCCESS;

}
