#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

#define   MAX   0x20

int main (int argc, char *argv[]) {

    char car = 'B';

    printf ("Caracter: %c\n", car);
    printf ("Caracter: %c\n", 'B');
    printf ("Caracter: %c\n", 0x42);

    printf ("\n");

    printf ("Cadena 1\n");
    printf ("Cadena 1 Cadena 2\n");
    printf ("Cadena 1"
            " "      "Cadena 2\n");

    printf ("\n");

    printf (
    "Usage:     letters [options]\n"
    "Prints letters.\n"
    );

    printf ("\n");

    printf ("\
            Usage:     letters [options]  \n\
            Prints Characters.            \n\
                                          \n\
            Options:                      \n\
");


    printf ("\n");

    printf ("Nombre: %s\n", "Txema");

    const char * name = "Txema";
    printf ("Nombre: %s\n", name);

    char mi_nombre[MAX];
    char hexa[MAX];

    printf ("Hexa Number: ");
    scanf (" %8[0-9a-fA-F]", hexa);
    __fpurge (stdin);
    printf ("Hexa: %s\n", hexa);

    printf ("Nombre: ");
    scanf (" %[^\n]", mi_nombre);
    printf ("Nombre: %s\n", mi_nombre);
    __fpurge (stdin);

    char * mn;
    printf ("Nombre: ");
    scanf (" %m[^\n]", &mn);
    printf ("Nombre: "
            "\x1B[33m" "%s" "\x1B[0m"
            ".\n", mn);
    free (mn);


    return EXIT_SUCCESS;
}
