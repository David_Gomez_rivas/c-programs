#include <stdio.h>
#include <stdlib.h>

void title () {
    system ("clear");
    system ("toilet -fpagga --gay 'JUSTIFY'");
    printf ("\n\n\n\n");
}

void preguntar (double *real, int *entero){
    printf ("Entero: ");
    scanf  (" %i",  entero);

    printf ("Real:   ");
    scanf  (" %lf", real);
}

int main() {

    int     entero;
    double  real;

    title ();

    preguntar (&real, &entero);

    printf ("Entero: %8i\n", entero);         // Justificación de tamaño fijo.
    printf ("Entero: %*i\n", 8, entero);      // Justificación parametrizable.
    printf ("Entero: %08i\n", entero);        // Relleno con 0's.
    printf ("Real:   %08.4lf\n", real);       //


    return EXIT_SUCCESS;
}
